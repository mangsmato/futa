<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class P_model extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}

	
	function p_login($username,$password){
		$this->db->where('player_id',$username);
		$this->db->where('password',$password);
			$query=$this->db->get('player');
		if ($query->num_rows()>0) {
			foreach ($query->result() as $rows) {
				//add all data to session
			    $newdata = array(
			      'club_id'  => $rows->club,
			      'username'  => $rows->fname,
			      'player_id'  => $rows->player_id,
			      'logged_in'  => TRUE,
			    );
			}
			$this->session->set_userdata('player_sess',$newdata);
   			return true;
		}
		return false;
	}
	 public function nego_edit($id){
			$this->db->select('*');
			$this->db->from('negotiation');
			$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				
			}
			$p_data = array(
					'id' => $id
				);
			
			$this->session->set_userdata('p_nego_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function nego_edit_save()
    {
    	$nego_update = array(
	    	 	'selling_salary' => $this->input->post('selling_salary'), 
	    		'transaction_date' => date('Y-m-d H:i:s'),
	    		'p_reply' => 'NEGOTIATE'	    		
	    	);
    	 $p_nego = $this->session->userdata('p_nego_sess');
    	 $this->db->where('id', $p_nego['id']);
	     
	    	$result = $this->db->update('negotiation',$nego_update);
	    	
    	 
    	
    	if ($result) {
    		return TRUE;
    	}
    	else{
    		return FALSE;
    	}
    }
	}
?>