<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class M_model extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}
		function p_save(){
		$m_session = $this->session->userdata('session_username');
			$club = $m_session['club_name'];
			$id_start = strtoupper(substr($club, 0,3));
			$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			$phone = $this->input->post('phone');
			$nationality = $this->input->post('nationality');
			$email = $this->input->post('email');
			$dob = $this->input->post('dob');
			$position = $this->input->post('position');
			$salary = $this->input->post('salary');
			$duration = $this->input->post('duration');
			$p_club = $this->input->post('p_club');
			$jazz_name = $this->input->post('jazz_name');
			$jazz_no = $this->input->post('jazz_no');
			$value = $this->input->post('value');
			$add = $mid=$p_id="";

			$this->db->where('club',$club);
			$this->db->order_by("date", "desc");
			$this->db->limit(1);
			$query = $this->db->get('player');
		if ($query->num_rows()>0) {			
				foreach ($query->result() as $row) {
					$player_id = $row->player_id;
				}
				$mid1 = substr($player_id, 4, -5);//substring -> string,start,end
				$mid = $mid1+1;
				if (strlen($mid) == 1) {
					$add = '000';
				}
				else if (strlen($mid) == 2) {
					$add = '00';
				}
				else if (strlen($mid) == 3) {
					$add = '0';
				}
				else{
					$add = '';
				}
				$p_id = $id_start. '/' . $add . $mid . '/' . date('Y');
		}
		else{
			$p_id=$id_start. '/' . '0001' . '/' . date('Y');
		}
		
		$insert_data = array(
			'fname' => ucwords($fname),
			'lname' => ucwords($lname), 
			'player_id' => $p_id,
			'position' => $position,
			'nationality' => ucwords($nationality),
			'dob'=>$dob,
			'club' => ucwords($club),
			'previous_club' => ($p_club),
			'password' => md5('123456'),
			'jazz_name' => ucwords($jazz_name),
			'jazz_no' => $jazz_no,
			'phone' => $phone,
			'email' => $email,
			'value' => $value,
			'date' => date('Y-m-d H:i:s')
		);

		$insert_contract = array(
			'player_id' => $p_id,
			'club' => ucwords($club),
			'salary' => $salary,
			'contract_start' => date('Y-m-d H:i:s'),
			'duration' => $duration
			 );
		$result = $this->db->insert('player',$insert_data);
		$result .= $this->db->insert('contract',$insert_contract);
		if ($result) {
				return true;
		}
		else{
			return false;
		}
	}
	function p_trans_save(){
		$m_session = $this->session->userdata('session_username');
			$club = $m_session['club_name'];
			$id_start = strtoupper(substr($club, 0,3));
			$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			$phone = $this->input->post('phone');
			$nationality = $this->input->post('nationality');
			$email = $this->input->post('email');
			$dob = $this->input->post('dob');
			$position = $this->input->post('position');
			$salary = $this->input->post('salary');
			$duration = $this->input->post('duration');
			$p_club = $this->input->post('p_club');
			$jazz_name = $this->input->post('jazz_name');
			$jazz_no = $this->input->post('jazz_no');
			$value = $this->input->post('value');
			$add = $mid=$p_id="";

			$this->db->where('club',$club);
			$this->db->order_by("date", "desc");
			$this->db->limit(1);
			$query = $this->db->get('player');
		if ($query->num_rows()>0) {			
				foreach ($query->result() as $row) {
					$player_id = $row->player_id;
				}
				$mid = substr($player_id, 4, -5);//substring -> string,start,end
				$mid = $mid+1;
				if (strlen($mid) == 1) {
					$add = '000';
				}
				else if (strlen($mid) == 2) {
					$add = '00';
				}
				else if (strlen($mid) == 3) {
					$add = '0';
				}
				else{
					$add = '';
				}
				$p_id = $id_start. '/' . $add . $mid . '/' . date('Y');
		}
		else{
			$p_id=$id_start. '/' . '0001' . '/' . date('Y');
		}
			
		$insert_data = array(
			'fname' => ucwords($fname),
			'lname' => ucwords($lname), 
			'player_id' => $p_id,
			'position' => $position,
			'nationality' => ucwords($nationality),
			'dob'=>$dob,
			'club' => ucwords($club),
			'previous_club' => ($p_club),
			'password' => md5('123456'),
			'jazz_name' => ucwords($jazz_name),
			'jazz_no' => $jazz_no,
			'phone' => $phone,
			'email' => $email,
			'value' =>$value,
			'date' => date('Y-m-d H:i:s')
		);

		$insert_contract = array(
			'player_id' => $p_id,
			'club' => ucwords($club),
			'salary' => $salary,
			'contract_start' => date('Y-m-d H:i:s'),
			'duration' => $duration
			 );
		// echo $p_id;
		$trans_update = array('status' => 'COMPLETE', 'date_completed' => date('Y-m-d H:i:s'));

		$trans_player = $this->session->userdata('p_update_sess');

		$this->db->where('player_id',$trans_player);
		$result = $this->db->update('player',$insert_data);

		$this->db->where('player_id',$trans_player);
		$result .= $this->db->update('contract',$insert_contract);

		$this->db->where('player_id',$trans_player);
		$result .= $this->db->update('transfer',$trans_update);
		if ($result) {
				return true;
		}
		else{
			return false;
		}
	}
	

		function m_login($username,$password){
		$this->db->select('*');
		$this->db->from('manager');
		$this->db->join('club','manager.club=club.name');
		 // $this->db->where('fname', $search);
		 // $this->db->or_where('lname', $search);
		 $this->db->where('manager.uname', $username);
		 $this->db->where('manager.password', $password);
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $rows) {
				//add all data to session
			    $newdata = array(
			      'uname'  => $rows->uname,
			      'username'  => $rows->lname,
			      'club_name' =>$rows->name,
			      'logged_in'  => TRUE,
			    );
			}
			$this->session->set_userdata('session_username',$newdata);
   			return true;
		}
		return false;
	}
	public function p_prof_edit($id)
	{
		$query = $this->db->query("SELECT * FROM player INNER JOIN contract ON contract.player_id=player.player_id 
                            WHERE player.id='$id'");
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$player_id = $row->player_id;
			$data[]=$row;
		}
		$this->session->set_userdata('prof_sess',$player_id);
		return $data;
		}
		else{
			return false;
		}
		
	}
	public function p_get($player){
			$query = $this->db->get_where('player', array('player_id' => $player));
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$p_data = array(
					'player' => $row->player_id, 
					'club' => $row->club
					);
				$data[] = $row;
			}
			
			$this->session->set_userdata('p_transfer_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
		public function p_buy_apply($id){
			$this->db->select('*');
			$this->db->from('transfer');
			$this->db->join('player','transfer.player_id=player.player_id');
			$this->db->where('transfer.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				$p_data = array(
					'player_id' => $row->player_id, 
					'source_club' => $row->transfer_club, 
					'release_close' => $row->release_close
					);
			}
			
			$this->session->set_userdata('p_tran_apply_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function p_add_transfer()
    {
    	$p_trans = $this->session->userdata('p_transfer_sess');
    	$query = $this->db->get_where('transfer', array('player_id' => $p_trans['player']));
    	if ($query->num_rows()>0) {
    		return FALSE;
    	}
    	else{
	    	$p_trans_save = array(
	    		'player_id' => $p_trans['player'], 
	    		'transfer_club' => $p_trans['club'],
	    		'transfer_type' => $this->input->post('transfer_type'),
	    		'release_close' => $this->input->post('release_close'),
	    		'status' => 'INCOMPLETE'
	    		);
	    	$success = $this->db->insert('transfer',$p_trans_save);
	    	// $success .= $this->db->insert('negotiation',$p_trans_save);
	    	if ($success) {
	    		return TRUE;
	    	}
	    	else{
	    		return FALSE;
	    	}
	    }
    }
    public function p_update_transfer()
    {
    	$admin = $this->session->userdata('session_username');
    	$club = $admin['club_name'];
    	$p_apply = $this->session->userdata('p_tran_apply_sess');
    	 $this->db->where('player_id', $p_apply['player_id']);
	     $this->db->where('dest_club', $club);
	    	 $query = $this->db->get('negotiation');
	    	 if ($query->num_rows()>0) {
	    	 	return FALSE;
	    	 }
	    	 else{
    	 	$buy_insert = array(
	    	 	'player_id' => $p_apply['player_id'],
	    	 	'source_club' => $p_apply['source_club'],
	    	 	'dest_club' => $club,
	    	 	'selling_price' =>  $p_apply['release_close'],
	    		'buying_price' => $this->input->post('buying_price'), 
	    		'transaction_date' => date('Y-m-d H:i:s'),
	    		'b_reply' => 'START',	    		
	    		'level' => 'MANAGERIAL'
	    		
	    	);
	    	$result = $this->db->insert('negotiation',$buy_insert);
	    	if ($result) {
	    		return TRUE;
	    	}
	    	else{
	    		return FALSE;
	    	}
	    	}
    	 
    	
    	
    }
    public function nego_edit($id){
			$this->db->select('*');
			$this->db->from('negotiation');
			$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				
			}
			$p_data = array(
					'id' => $id
				);
			
			$this->session->set_userdata('nego_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function buy_edit($id){
			$this->db->select('*');
			$this->db->from('negotiation');
			$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				
			}
			$p_data = array(
					'id' => $id
				);
			
			$this->session->set_userdata('buy_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}

	public function nego_edit_save()
    {
    	$nego_update = array(
	    	 	'selling_price' => $this->input->post('release_close'), 
	    		'transaction_date' => date('Y-m-d H:i:s'),
	    		's_reply' => 'NEGOTIATE'	    		
	    	);
    	 $p_nego = $this->session->userdata('nego_sess');
    	 $this->db->where('id', $p_nego['id']);
	     
	    	$result = $this->db->update('negotiation',$nego_update);
	    	
    	 
    	
    	if ($result) {
    		return TRUE;
    	}
    	else{
    		return FALSE;
    	}
    }
    	public function buy_edit_save()
    {
    	$buy_update = array(
	    	 	'buying_price' => $this->input->post('buying_price'), 
	    		'transaction_date' => date('Y-m-d H:i:s'),
	    		'b_reply' => 'NEGOTIATE'	    		
	    	);
    	 $p_buy = $this->session->userdata('buy_sess');
    	 $this->db->where('id', $p_buy['id']);
	     
	    	$result = $this->db->update('negotiation',$buy_update);
	    	
    	 
    	
    	if ($result) {
    		return TRUE;
    	}
    	else{
    		return FALSE;
    	}
    }
    public function list_edit($id){
			$this->db->select('*');
			$this->db->from('transfer');
			$this->db->join('player','player.player_id=transfer.player_id');
			$this->db->where('transfer.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				
			}
			$p_data = array(
					'id' => $id
				);
			
			$this->session->set_userdata('list_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function list_edit_save()
    {
    	$nego_update = array(
	    	 	'release_close' => $this->input->post('release_close'),
	    	 	 'transfer_type' => $this->input->post('transfer_type')
	    	);
    	 $p_list = $this->session->userdata('list_sess');
    	 $this->db->where('id', $p_list['id']);
	     
	    	$result = $this->db->update('transfer',$nego_update);
    	if ($result) {
    		return TRUE;
    	}
    	else{
    		return FALSE;
    	}
    }
    public function buy_ok($id){
			$this->db->select('*');
			$this->db->from('negotiation');
			$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				
			}
			$p_data = array(
					'id' => $id
				);
			
			$this->session->set_userdata('p_level_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
		public function buy_ok_save()
    {
    	
    	$buy_update = array(
	    	 	'buying_salary' => $this->input->post('buying_salary'), 
	    		'transaction_date' => date('Y-m-d H:i:s'),	    		
	    		'level' => 'PLAYER',
	    		'm_reply' => 'START'    		
	    	);
    	 $p_buy = $this->session->userdata('p_level_sess');
    	 $this->db->where('id', $p_buy['id']);
	     
	    	$result = $this->db->update('negotiation',$buy_update);
	    	
    	 
    	
    	if ($result) {
    		return TRUE;
    	}
    	else{
    		return FALSE;
    	}
    }
    public function salary_edit($id){
			$this->db->select('*');
			$this->db->from('negotiation');
			$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				
			}
			$p_data = array(
					'id' => $id
				);
			
			$this->session->set_userdata('salary_nego_sess',$p_data);
			return $data;
		}
		else{
			return 0;
		}
	}
    public function salary_nego_save()
    {
    	$nego_update = array(
	    	 	'buying_salary' => $this->input->post('buying_salary'), 
	    		'transaction_date' => date('Y-m-d H:i:s'),
	    		'm_reply' => 'NEGOTIATE'	    		
	    	);
    	 $p_nego = $this->session->userdata('salary_nego_sess');
    	 $this->db->where('id', $p_nego['id']);	     
	    	$result = $this->db->update('negotiation',$nego_update);
	    	
    	 
    	
    	if ($result) {
    		return TRUE;
    	}
    	else{
    		return FALSE;
    	}
    }
}
?>