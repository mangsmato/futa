<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class A_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function a_login($username,$password){
		$this->db->where('username',$username);
		$this->db->where('password',$password);

		$query=$this->db->get('admin');
		if ($query->num_rows()>0) {
   			return true;
		}
		return false;
	}
	function c_reg(){
		// $query = $this->db->get_where('club', array('name' => $club_name), 1);
		$insert_data = array(
			'name' => ucwords($this->input->post('club_name')),
			'stadium' =>  ucwords($this->input->post('stadium')),
			'reg_date' => date('Y-m-d H:i:s'),
			'form_yr' => $this->input->post('form_yr'),
			'address' =>  ucwords($this->input->post('address')),
			'city' =>  ucwords($this->input->post('city')),
			'club_phone' =>  $this->input->post('phone'),
			'club_email' =>  $this->input->post('email'),

			);
		
			$result = $this->db->insert('club',$insert_data);
		if ($result) {
			$this->session->set_userdata('club_reg_sess',$insert_data);
				return true;
		}
		else{
			return false;
		}
	}
		 public function  c_get($id){	
		 $this->db->where('club.club_id', $id);
		 $query = $this->db->get('club');
    	if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$club_data = array(
					'club' => $row->name,
					'stadium' => $row->stadium
				);
				$data[] = $row;
			}
			
			$this->session->set_userdata('club_sess',$club_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function  c_id_update($id){	
		 $this->db->select('*');
    	$this->db->from('club');
    	$this->db->join('owner','club.name=owner.club');
    	$this->db->join('partners','club.name=partners.club');
    	$this->db->where('club_id',$id);
    	$query = $this->db->get();
    	if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
									$club = $row->name;
				
				$data[] = $row;
			}
			
			$this->session->set_userdata('club_update_sess',$club);
			return $data;
		}
		else{
			return 0;
		}
	}
	function get_clubs() {    
		$club_name=array();
	    $this->db->select('name');
	    $this->db->from('club');
	    $this->db->order_by('name');
	    $query = $this->db->get();
	    if ($query->num_rows() > 0) {
	        foreach ($query->result_array() as $row) {
	            $club_name[] = $row;
	        }
	    }
	    return $club_name;
	}
	public function stake_reg($club)
	{
		$own_array = array(
			'club' =>$club , 
			'own_name' =>ucwords($this->input->post('own_name')) ,
			'own_shares' =>$this->input->post('own_shares') ,
			'own_phone' =>$this->input->post('own_phone') ,
			);
		$part_array = array(
			'club' =>$club , 
			'part_name' =>ucwords($this->input->post('part_name')) ,
			'part_shares' =>$this->input->post('part_shares') ,
			'part_phone' =>$this->input->post('part_phone') ,
			);
		$result = $this->db->insert('owner',$own_array);
		$result .= $this->db->insert('partners',$part_array);
		if ($result) {
			return TRUE;
		}
		else{
			return FALSE;
		}

	}


		public function m_update(){
     	$club_name = $this->session->userdata('manager_sess');
     	$club = $club_name['club'];
     	$query =  $this->db->get_where('manager',array('club' => $club ));
     	
     	if ($query->num_rows()>0) {
     	$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$phone = $this->input->post('m_phone');
		$email = $this->input->post('m_email');
		// $club = $this->input->post('club');
		$password = md5($this->input->post('password'));

		$update_data = array(
			'fname' => ucwords($fname),
			'lname' => ucwords($lname), 
			'phone' => $phone,
			'email' => $email,
			'club' => ucwords($club),
			'password' => $password
			);
		
			$this->db->where("phone = '$phone' AND club != '$club' OR email = '$email' AND club != '$club'");
			$query1 = $this->db->get('manager');
			if ($query1->num_rows()>0) {
				return false;
			}
			else{
				$this->db->where('club', $club);
				$result = $this->db->update('manager', $update_data); 
				return true;
			}
		}
		else{
			$result = $this->m_reg($club);
			if ($result) {
				return TRUE;
			}
			else{
				return false;
			}
		}
	}
	function m_reg($club){
		   

		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		// $uname = $this->input->post('uname');
		$phone = $this->input->post('m_phone');
		$email = $this->input->post('m_email');
		$add = $id = $mid="";
		$this->db->order_by("add_date", "desc");
		$this->db->limit(1);
		$uname = "";
		$query = $this->db->get('manager');
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$uname=$row->uname;
			}
			$mid1 = substr($uname, 4,-5);
			$mid = $mid1+1;
				if (strlen($mid) == 1) {
					$add = '000';
				}
				else if (strlen($mid) == 2) {
					$add = '00';
				}
				else if (strlen($mid) == 3) {
					$add = '0';
				}
				else{
					$add = '';
				}
			$uname='KFF/'.$add.$mid.'/'.date('Y');
		}
		else{
			$uname='KFF/'.'0001'.'/'.date('Y');
		}	
		$insert_data = array(
			'fname' => ucwords($fname),
			'lname' => ucwords($lname), 
			'uname' => ucwords($uname),
			'phone' => $phone,
			'email' => $email,
			'club' => ucwords($club),
			'password' => md5('123456'),
			'add_date' => date('Y-m-d H:i:s')
			);
		
			$result = $this->db->insert('manager',$insert_data);
			if ($result) {
				return true;
			}
			else{
				return false;
			}
			
		
		
	}
		 public function m_get($id){	
    	$this->db->select('*');
		$this->db->from('club');
		$this->db->join('manager','manager.club=club.name','left');
		$this->db->where('manager.id', $id);
		$query = $this->db->get();
    	if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$club_data = array(
					'fname' => $row->fname, 
					'lname' => $row->lname, 
					'phone' => $row->phone, 
					'email' => $row->email, 
					'uname' => $row->uname, 
					'club' => $row->name
					);
				$data[] = $row;
			}
			
			$this->session->set_userdata('manager_sess',$club_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function m_assign($id){	
    	$this->db->select('*');
		$this->db->from('club');
		$this->db->join('manager','manager.club=club.name','left');
		$this->db->where('club_id', $id);
		$query = $this->db->get();
    	if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$club_data = array(
					'fname' => $row->fname, 
					'lname' => $row->lname, 
					'phone' => $row->phone, 
					'email' => $row->email, 
					'uname' => $row->uname, 
					'club' => $row->name
					);
				$data[] = $row;
			}
			
			$this->session->set_userdata('manager_sess',$club_data);
			return $data;
		}
		else{
			return 0;
		}
	}
	public function c_delete($id)
	{
		$result = $this->db->delete('club', array('club_id' => $id));
		if ($result) {
			return true;
		}
		else{
			return false;
		}
	}
	public function m_delete($id)
	{
		$result = $this->db->delete('manager', array('id' => $id));
		if ($result) {
			return true;
		}
		else{
			return false;
		}
	}
	 public function report_clubs()
    {
    	$radio = $this->input->post('search');
    	if ($radio=='all') {
    		$this->db->select("*");
		$this->db->from('club');
		$this->db->join('manager','manager.club=club.name','left');
		// $this->db->join('manager','manager.club=club.name','left');
		
    	}
    	else if($radio=='date'){
    		$start = $this->input->post('start');
    		$end = $this->input->post('end');
    		if (empty($start)||empty($end)) {
    			return false;
    		}
    		else{
    			$this->db->select("*");
				$this->db->from('club');
				$this->db->join('manager','manager.club=club.name','left');
				$this->db->where("club.reg_date BETWEEN '$start' AND '$end'");
				
    		}
    	}
    	
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else{
			return false;
		}
	}
	 public function report_managers()
    {
    	$radio = $this->input->post('search');
    	if ($radio=='all') {
    		$this->db->select("*");
		$this->db->from('manager');
		$this->db->join('club','manager.club=club.name');
		
    	}
    	else if($radio=='date'){
    		$start = $this->input->post('start');
    		$end = $this->input->post('end');
    		if (empty($start)||empty($end)) {
    			return false;
    		}
    		else{
    			$this->db->select("*");
				$this->db->from('manager');
				$this->db->join('club','manager.club=club.name');
				$this->db->where("manager.add_date BETWEEN '$start' AND '$end'");
				
    		}
    	}
    	
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else{
			return false;
		}
	}
	 public function report_players()
    {
    	$radio = $this->input->post('search');
    	if ($radio=='all') {
		$query = $this->db->get('player');
    	}
    	else if($radio=='date'){
    		$club = $this->input->post('club');
    		if (empty($club)) {
    			return false;
    		}
    		else{
    			$query = $this->db->get_where('player',array('club' => $club ));
				
    		}
    	}
    	
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else{
			return false;
		}
	}	
	 public function report_transfers($radio,$transfer_type,$level,$status)
    {
    	if ($radio=='all') {
    		$query = $this->db->query("SELECT * FROM transfer
						JOIN transfer_listed ON transfer.player_id=transfer_listed.player_id
						JOIN negotiation ON transfer.player_id=negotiation.player_id");
		
    	}
    	else if($radio=='t_type'){
    			$query = $this->db->query("SELECT * FROM transfer
						JOIN transfer_listed ON transfer.player_id=transfer_listed.player_id
						JOIN negotiation ON transfer.player_id=negotiation.player_id
						WHERE transfer_type='$transfer_type'");
    	}
    	else if($radio=='t_level'){
    		$query = $this->db->query("SELECT * FROM transfer
						JOIN transfer_listed ON transfer.player_id=transfer_listed.player_id
						JOIN negotiation ON transfer.player_id=negotiation.player_id
						WHERE level='$level'");
    	}
    	else if($radio=='t_status'){
    		$query = $this->db->query("SELECT * FROM transfer
						JOIN transfer_listed ON transfer.player_id=transfer_listed.player_id
						JOIN negotiation ON transfer.player_id=negotiation.player_id
						WHERE status='$status'");
    	}
    	
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		else{
			return false;
		}
	}	
}
	

?>