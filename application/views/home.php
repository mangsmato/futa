<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</head>
<body background="<?php echo base_url();?>images/futa.jpg">
	<div class="container" id="login">
		<div class="col-sm-4 col-sm-offset-3">	
			<div class="panel panel-default">
			  	<div class="panel-heading">KFF Online Transfer System</div>
			  	<?php #echo date('Y-m-d H:i:s');
			  			#echo date_default_timezone_get();
			  		?>
			  	<div class="panel-body">
			  		<div class="form-group">
			  			<a href="<?=base_url('a')?>" class="btn btn-link">Administrator Login</a>
			  		</div>
			  		<div class="form-group">
			  			<a href="<?=base_url('m')?>" class="btn btn-link">Club Manager Login</a>
			  		</div>
			  		<div class="form-group">
			  			<a href="<?=base_url('p')?>" class="btn btn-link">Player Login</a>
			  		</div>
			  		
					   <?php echo $this->session->flashdata('error');?>
			  	</div>
		  </div>
		</div>  
	</div>

	   
</body>
</html>
<script src="assets/js/bootstrap.min.js"></script>
