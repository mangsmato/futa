<!DOCTYPE html>
<html>
<head>
    <title>Contact</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</head>
<body background="<?php echo base_url();?>images/futa.jpg">
<div id="page-wrapper">
        <div class="row col-sm-8" id="contact">
            <div class="panel panel-default">
            <div class="panel-heading">Feel free to reach to us: Triecom Technologies - KFF System Developers</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('c_ctlr/send_email'); ?>
                             <form role="form">
                                    <div class="form-group ">
                                            <label for="inputEmail">Your Name:</label>
                                            <input type="text" class="form-control" id="inputEmail" placeholder=" lukongo diverse" name="name"  required>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputEmail">Your Email:</label>
                                            <input type="email" class="form-control" id="inputEmail" placeholder="luko@gmail.com" name="email" required >
                                        </div> 
                                        <div class="form-group ">
                                            <label for="inputEmail">Phone Number:</label>
                                            <input type="number" class="form-control" id="inputEmail" placeholder="0715889537" name="phone" required min="0">
                                        </div> 
                                        <div class="form-group ">
                                            <label for="inputEmail">Subject:</label>
                                            <input type="text" class="form-control" id="inputEmail" placeholder="system maintenance" name="subject" required>
                                        </div> 
                                        <div class="form-group ">
                                            <label for="inputEmail">Message:</label>
                                            <textarea name="message" class="form-control" rows="3" required></textarea>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-offset-8 col-sm-4">
                                                <button type="submit" class="btn btn-primary">SEND
                                                        <span class="glyphicon glyphicon-save"></span>
                                                </button>
                                            </div>
                                        </div>                                                          
                                    </form>
                            <?php form_close();?>
                            <h6> <a href="http://www.triecomtechnologies.com" target="_blank">Triecom Technologies</a> call: 0716432211</h6>
                </div>
            </div>
    </div>

</div>
</body>
</html>

