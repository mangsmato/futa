<div id="page-wrapper" style="width:83%; margin-left:0.5em;">
    <h1>Update Manager Information</h1>
            <div class="panel panel-default">
            <div class="panel-heading">Registered Managers</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($c_edit_error)) ? $c_edit_error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                                              
                            <?php
                                $query = $this->db->query("SELECT * FROM manager");
                                if ($query->num_rows()>0) {
                                    echo '<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Manager Name</th>
                                          <th>Username</th>
                                          <th>Phone Number</th>
                                          <th>Email Address</th>
                                          <th>Registration Date</th>
                                          <th>Club</th>
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                        foreach ($query->result() as $row ) {
                                            $no++;
                                            echo '<tr class="odd gradeX">';
                                            echo "<td>" . $no. "</td>";
                                            echo "<td>" . $row->fname. " " . $row->lname. "</td>";
                                            echo "<td>" . $row->uname. "</td>";
                                            echo "<td>" . $row->phone . "</td>";
                                            echo "<td>" . $row->email ."</td>";
                                            echo "<td>" . $row->add_date ."</td>";
                                            echo "<td>" . $row->club ."</td>";
                                            echo '<td>'.
                                            anchor("a_ctlr/m_get/".$row->id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit Manager?')")). 
                                            anchor("a_ctlr/m_delete/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Remove Manager?')")) .
                                            '</td>';
                                            echo "</tr>";
                                        }
                                        echo '</tbody>';
                                        echo "</table>";
                                    }
                                    else{
                                        echo '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <strong>you have not registered any manager</strong>';
                                    }                         
                            
                            ?>
                            
                </div>
    </div>
</div>

