<?php
$club_name=$stadium=$address=$club_phone=$city=$club_email=$own_name=$own_phone=$own_shares=$part_name=$form_yr=$part_phone=$part_shares="";
    if (isset($result)) {
         foreach ($result as $row) {
            $club_name = $row->name;
            $stadium = $row->stadium;
            $address = $row->address;
            $city = $row->city;
            $form_yr=$row->form_yr;
             $club_phone = $row->club_phone;
             $club_email = $row->club_email;
              $own_name = $row->own_name;
              $own_phone = $row->own_phone;
              $own_shares = $row->own_shares;
              $part_name = $row->part_name;
              $part_phone = $row->part_phone;
              $part_shares = $row->part_shares;

         }
         // $this->session->set_userdata('club_update_sess',$club_name);
     }
?>
<div id="page-wrapper">
    <h1>Club Registration/Club  Update</h1>

            <div class="panel panel-default">
            <div class="panel-heading">Update Club Information</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/c_update'); ?>
                             <form role="form">
                                   
                                   <fieldset>
                                       <legend>Club Details</legend>
                                          <div class="form-group col-sm-4 ">
                                                                <label for="inputEmail">Club Name</label>
                                                                <input type="text" class="form-control" id="inputEmail"  name="club_name" value="<?php echo $club_name;?>"  required>
                                                            </div>
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Home Stadium</label>
                                                                <input type="text" class="form-control" id="inputEmail"  name="stadium" value="<?php echo $stadium;?>" required>
                                                            </div> 
                                                            <div class="form-group col-sm-4">
                                                                <label for="dob">Year of Formation</label>
                                                                <div class="input-group">
                                                                    <input id="dob" name="form_yr" type="text" class="form-control" required readonly value="<?php echo $form_yr;?>">
                                                                    <span class="input-group-btn">
                                                                        <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('dob','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Address</label>
                                                                <input type="text" class="form-control" id="inputEmail" name="address" value="<?php echo $address;?>"  required>
                                                            </div> 
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">City</label>
                                                                <input type="text" class="form-control" id="inputEmail"  name="city"  value="<?php echo $city;?>" required>
                                                            </div> 
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Club Email</label>
                                                                <input type="email" class="form-control" id="inputEmail"  name="email" value="<?php echo $club_email;?>" required>
                                                            </div> 
                                                             <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Club Telephone</label>
                                                                <input type="tel" class="form-control" id="inputEmail"  name="phone" onkeypress="return numbersonly(event)" 
                                                                onkeyup="return limitlength(this, 15)" value="<?php echo $club_phone;?>" required>
                                                            </div>
                                   </fieldset>
                                   <fieldset>
                                   <legend> Club Owners</legend>
                                    <div class="form-group col-sm-4 ">
                                    <label for="fname">Name</label>
                                    <input type="text" class="form-control"  name="own_name" value="<?php echo $own_name;?>" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="lname">Shares(%)</label>
                                        <input type="number" class="form-control" name="own_shares" value="<?php echo $own_shares;?>" min="0" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone Contact</label>
                                    <input type="tel" class="form-control"  name="own_phone" onkeypress="return numbersonly(event)"
                                     onkeyup="return limitlength(this, 15)" value="<?php echo $own_phone;?>" required>
                                </div>
                                </fieldset>
                                      <fieldset>
                                          <legend>Partners</legend>
                                          <div class="form-group col-sm-4 ">
                                    <label for="fname">Name</label>
                                    <input type="text" class="form-control"  name="part_name" value="<?php echo $part_name;?>" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="lname">Shares(%)</label>
                                        <input type="number" class="form-control"  name="part_shares" min="0" value="<?php echo $part_shares;?>" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone Contact</label>
                                    <input type="tel" class="form-control" name="part_phone" onkeypress="return numbersonly(event)" 
                                    onkeyup="return limitlength(this, 15)" value="<?php echo $part_phone;?>" required>
                                </div>
                                      </fieldset>

                                        <div class="form-group">
                                            <div class="col-sm-offset-8 col-sm-4">
                                            <a href="<?=base_url('c_edit')?>" type="button" class="btn btn-default">CANCEL</a>
                                                <button type="submit" class="btn btn-primary">SAVE
                                                        <span class="glyphicon glyphicon-save"></span>
                                                </button>
                                            </div>
                                        </div>                                                          
                                    </form>
                            <?php form_close();?>
                </div>
            </div>
   
</div>

