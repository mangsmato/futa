<?php
$club_name = $fname=$lname=$phone=$uname=$email=$stadium="";
    if (isset($edit_club)) {
        if (is_array($edit_club)) {
            
        
         foreach ($edit_club as $row) {
            $club_name = $row->name;
            $fname = $row->fname;
            $lname = $row->lname;
            $phone = $row->phone;
            $uname = $row->uname;
            $email = $row->email;
            $stadium = $row->stadium;
         }
     }
     }
?>
<div id="page-wrapper">
    <h1>Club Updates</h1>
        <div class="row">
            <div class="panel panel-default">
            <div class="panel-heading">Update Manager Details</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($c_success)) ? $c_success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($c_error)) ? $c_error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/m_update'); ?>
                             <form role="form" class="form form-horizontal">
                                <fieldset>
                                    <legend>Club Details</legend>
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">Club</label>                
                                         <p class="form-control-static"><?php echo $club_name; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                    <label for="fname">Stadium</label>
                                    <p class="form-control-static"><?php echo $stadium; ?></p>
                                </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Manager Details</legend>
                                    <div class="form-group col-sm-4 ">
                                        <label for="fname">First Name</label>
                                        <input type="text" class="form-control"  placeholder="Walikhe" name="fname" value="<?php echo $fname;?>" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="lname">Last Name</label>
                                        <input type="text" class="form-control" placeholder="Sikoli" name="lname" value="<?php echo $lname;?>" required>
                                    </div>
                                  
                                    <div class="form-group col-sm-4 ">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" placeholder="0767652612" name="m_phone" value="<?php echo $phone;?>" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 15)"  required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="phone">Email Address</label>
                                        <input type="email" class="form-control" placeholder="som@ex.com" name="m_email" value="<?php echo $email;?>" >
                                    </div>
                                     
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">Username</label>                
                                         <p class="form-control-static"><?php echo $uname; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" placeholder="*****" name="password" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="c_password">Confirm Password</label>
                                        <input type="password" class="form-control" placeholder="*****" name="c_password" required>
                                    </div>
                                </fieldset>
                               
                                
                               
                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-4">
                                     <a href="<?=base_url('m_manage_disp')?>" type="button" class="btn btn-default">CANCEL</a>
                                        <button type="submit" class="btn btn-primary">SAVE
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                    </div>
                                </div>                                                          
                           </form>
                           
                           
                            <?php echo form_close();?>
                </div>
            </div>
    </div>
</div>
