 <script type="text/javascript">// <![CDATA[
function hide() {
    document.getElementById('hide_date').style.display = 'none';
}
function show() {
    document.getElementById('hide_date').style.display = 'block';
}

    </script>
<div id="page-wrapper" style="width: 80%;">
    <h1>Manager Reports</h1>
            <div class="panel panel-default">
            <div class="panel-heading">Registered Managers</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/report_managers'); ?>
                             <form role="form" class="form-horizontal col-sm-12" >
                             
                                    <div class="form-group col-sm-3">
                                            <label for="inputEmail">All Managers</label>
                                            <input type="radio" value="all" name="search" onclick="hide()" required>
                                           
                                        </div>
                                        <div class="form-group col-sm-4">
                                             <label for="inputEmail">Search by Registration Date</label>
                                            <input type="radio" value="date" name="search" onclick="show()" required>
                                             <div id="hide_date" style="display:none;">
                                        <div class="form-group">
                                            <label for="dob">From:</label>
                                            <input id="start_date" name="start" type="text" readonly>
                                            <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('start_date','yyyyMMdd','dropdown',true,'24',true)" style="cursor:pointer"/>
                                        </div>
                                        <div class="form-group ">
                                            <label for="dob">To:</label>
                                            <input id="stop_date" name="end" type="text" readonly>
                                            <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('stop_date','yyyyMMdd','dropdown',true,'24',true)" style="cursor:pointer"/>
                                        </div>
                                        </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary">Search
                                                        <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </div>
                                        </div>      
                                                                                           
                                    </form>
                                    <?php form_close();?> 
                         <div id="printable">
                            <?php
                            if (isset($managers)) {
                                                            
                                if (is_array($managers)) {
                                    echo '<div id="report_heading">';
                                    echo isset($heading) ? $heading : "" ;
                                    echo "</div>";
                                    echo '<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                          <th>No.</th>
                                          <th>Full Name</th>
                                          <th>Username</th>
                                          <th>Phone</th>
                                          <th>Email</th>
                                          <th>Club</th>
                                          <th>Registration Date</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                        foreach ($managers as $row ) {
                                            $no++;
                                            echo "<tr>";
                                            echo "<td>" . $no. "</td>";
                                            echo "<td>" . $row->fname . " " . $row->lname . "</td>";
                                            echo "<td>" . $row->uname. "</td>";
                                            echo "<td>" . $row->phone . "</td>";
                                            echo "<td>" . $row->email . "</td>";
                                            echo "<td>" . $row->name . "</td>";
                                            echo "<td>" . $row->add_date . "</td>";
                                            echo "</tr>";
                                        }
                                        echo '</tbody>';
                                        echo "</table>";
                                        echo "</div>";
                                         // echo form_open('a_ctlr/m_pdf');
                                            echo '<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary" onclick="printDiv()">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             // echo form_close();
                                }
                            }
                            ?>
                            
                </div>
            </div>
    </div>
