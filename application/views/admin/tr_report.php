 <script type="text/javascript">// <![CDATA[

function show_all() {
    document.getElementById('hide_type').style.display = 'none';
    document.getElementById('hide_level').style.display = 'none';
    document.getElementById('hide_status').style.display = 'none';
}
function show_type() {
    document.getElementById('hide_type').style.display = 'block';
    document.getElementById('hide_level').style.display = 'none';
    document.getElementById('hide_status').style.display = 'none';
}

function show_level() {
    document.getElementById('hide_type').style.display = 'none';
    document.getElementById('hide_level').style.display = 'block';
    document.getElementById('hide_status').style.display = 'none';
}
function show_status() {
    document.getElementById('hide_type').style.display = 'none';
    document.getElementById('hide_level').style.display = 'none';
    document.getElementById('hide_status').style.display = 'block';
}
    </script>
<div id="page-wrapper" style="width:76%;margin-top:-2em;">
    <h1>Transfer Reports</h1>
         <div class="panel panel-default">
            <div class="panel-heading">Transfers this season</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/report_transfers'); ?>
                             <form role="form" class="form-horizontal" >
                             
                                    <div class="form-group col-sm-1">
                                            <label for="inputEmail">All Transfers</label>
                                            <input type="radio" value="all" name="search" onclick="show_all()" required>
                                           
                                        </div>
                                        <div class="form-group col-sm-2">
                                             <label for="inputEmail">Search by Transfer Type</label>
                                            <input type="radio" value="t_type" name="search" onclick="show_type()" required>
                                             <div id="hide_type" style="display:none;">
                                            <select name="transfer_type" class="form-control">
                                                <option value="Sale">Sale</option>
                                                <option value="Loan">Loan</option>
                                                <option value="Free">Free</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="form-group col-sm-2">
                                             <label for="inputEmail">Search by Transfer Level</label>
                                            <input type="radio" value="t_level" name="search" onclick="show_level()" required>
                                             <div id="hide_level" style="display:none;">
                                            <select name="level" class="form-control">
                                                <option value="PLAYER">PLAYER</option>
                                                <option value="MANAGERIAL">MANAGERIAL</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="form-group col-sm-2">
                                             <label for="inputEmail">Search by Transfer status</label>
                                            <input type="radio" value="t_status" name="search" onclick="show_status()" required>
                                             <div id="hide_status" style="display:none;">
                                            <select name="status" class="form-control">
                                                <option value="COMPLETE">COMPLETE</option>
                                                <option value="INCOMPLETE">INCOMPLETE</option>
                                            </select>
                                        </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-primary">Search
                                                        <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </div>
                                        </div>      
                                                                                           
                                    </form>
                                    <?php form_close();?> 
                                    <div id="printable">
                        
                            <?php
                            if (isset($transfers)) {
                                                            
                                if (is_array($transfers)) {
                                    echo '<div id="report_heading">' . '<br><br>'.'<br><br>' ;
                                    echo isset($heading) ? $heading : "" ;
                                    echo "</div>";
                                    echo '<table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                          <th>No.</th>
                                          <th>Full Name</th>
                                          <th>From</th>
                                          <th>To</th>
                                          <th>Transfer Type</th>
                                          <th>Price(Ksh.)</th>
                                          <th>Salary(Ksh.)</th>
                                          <th>Level</th>
                                          <th>Status</th>
                                          </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                        foreach ($transfers as $row ) {
                                            $no++;
                                            echo "<tr>";
                                            echo "<td>" . $no. "</td>";
                                            echo "<td>" . $row->fname . " " . $row->lname . "</td>";
                                            echo "<td>" . $row->source_club . "</td>";
                                            echo "<td>" . $row->dest_club . "</td>";
                                             echo "<td>" . $row->transfer_type . "</td>";
                                            echo "<td>" . number_format($row->buying_price,2) . "</td>";
                                            echo "<td>" . number_format($row->buying_salary,2) . "</td>";
                                            echo "<td>" . $row->level . "</td>";
                                            echo "<td>" . $row->status . "</td>";
                                            echo "</tr>";
                                        }
                                        echo '</tbody>';
                                        echo "</table>";
                                        echo "</div>";
                                         // echo form_open('a_ctlr/tr_pdf');
                                            echo '<div class="col-sm-offset-10 col-sm-5">
                                                <button type="submit" class="btn btn-primary" onclick="printDiv()">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             // echo form_close();
                                }
                            }
                            ?>
                            
                </div>
            </div>
   
</div>