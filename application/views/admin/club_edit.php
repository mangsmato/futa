<div id="page-wrapper" style="margin-left:0.1em;width:83%;">
    <h1>Manage CLubs</h1>
            <div class="panel panel-default">
            <div class="panel-heading">Registered Clubs</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($c_edit_error)) ? $c_edit_error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                                              
                            <?php
                                $query = $this->db->query("SELECT * FROM club JOIN owner ON club.name=owner.club JOIN partners ON club.name=partners.club LEFT JOIN MANAGER ON club.name=manager.club");
                                if ($query->num_rows()>0) {
                                    echo '<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Club Name</th>
                                          <th>Stadium</th>
                                          <th>Registration Date</th>
                                          <th>Owner</th>
                                          <th>Partners</th>
                                          <th>Manager</th>
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                        foreach ($query->result() as $row ) {
                                            $no++;
                                            echo '<tr class="odd gradeX">';
                                            echo "<td>" . $no. "</td>";
                                            echo "<td>" . $row->name. "</td>";
                                            echo "<td>" . $row->stadium . "</td>";
                                            echo "<td>" . $row->reg_date ."</td>";
                                            echo "<td>" . $row->own_name. "</td>";
                                            echo "<td>" . $row->part_name. "</td>";
                                            echo "<td>" . $row->fname. " " . $row->lname. "</td>";
                                            echo '<td>'.
                                            anchor("a_ctlr/m_assign/".$row->club_id,'<img src="' . base_url() . 'images/user.png"/>',array('onclick' => "return confirm('Assign new manager?')")). 
                                            anchor("a_ctlr/c_id_update/".$row->club_id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit Club?')")). 
                                            anchor("a_ctlr/c_delete/".$row->club_id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Remove Club?')")) .
                                            '</td>';
                                            echo "</tr>";
                                        }
                                        echo '</tbody>';
                                        echo "</table>";
                                    }
                                    else{
                                        echo '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <strong>you have not registered any club</strong>';
                                    }                         
                            
                            ?>
                            
                </div>
    </div>
</div>

