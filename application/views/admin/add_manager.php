
<div id="page-wrapper">
           <div class="row">
            <div class="panel panel-default">
            <div class="panel-heading">New Manager</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($m_success)) ? $m_success : "" ;
                                 
                                ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($m_error)) ? $m_error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/m_reg'); ?>
                            
                                <form role="form" class="form form-horizontal">
                                <div class="form-group col-sm-4 ">
                                    <label for="fname">First Name</label>
                                    <input type="text" class="form-control"   name="fname" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="lname">Last Name</label>
                                    <input type="text" class="form-control"  name="lname" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone</label>
                                    <input type="number" class="form-control"  name="m_phone" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 15)" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="phone">Email Address</label>
                                    <input type="email" class="form-control" name="m_email">
                                </div>
                             <!--     <div class="form-group col-sm-4 ">
                                    <label for="inputEmail">Club</label>                
                                     <select class="selectpicker form-control" name="club" required> 
                                          
                                            <?php
                                                #foreach($clubs as $each)
                                                {
                                                    ?>
                                                    
                                                    <?php
                                                }
                                                ?>
                                    </select>   
                                </div> -->
                               <!--  <div class="form-group col-sm-4 ">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" placeholder="Sikoli" name="password" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="c_password">Confirm Password</label>
                                    <input type="password" class="form-control" placeholder="Sikoli" name="c_password" required>
                                </div> -->
                               
                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-4">
                                        <button type="submit" class="btn btn-primary">NEXT
                                            <span class="glyphicon glyphicon-arrow-right"></span>
                                        </button>
                                    </div>
                                </div>                                                          
                           </form>
                           
                           
                            <?php echo form_close();?>
                </div>
            </div>
    </div>
</div>
