
<div id="page-wrapper" style="width:80%; margin-top: -1.5em;">
    
            <div id="printable" style="margin-top: -1em;">
           <center><h3>KFF Club Owners</h3></center> 
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                                              
                            <?php
                                $this->db->select('*');
                                $this->db->from('owner');
                                $this->db->join('partners','owner.club=partners.club');
                                $query = $this->db->get();
                                if ($query->num_rows()>0) {
                                    
                                    echo '<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                          <th>No.</th>
                                          <th>Club Name</th>
                                          <th>Owner</th>
                                          <th>Shares</th>
                                          <th>Phone</th>
                                          <th>Partners</th>
                                          <th>Shares</th>
                                          <th>Phone</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                        foreach ($query->result() as $row ) {
                                            $no++;
                                            echo '<tr class="odd gradeX">';
                                            echo "<td>" . $no. "</td>";
                                            echo "<td>" . $row->club. "</td>";
                                            echo "<td>" . $row->own_name . "</td>";
                                            echo "<td>" . $row->own_shares ."</td>";
                                            echo "<td>" . $row->own_phone. "</td>";
                                            echo "<td>" . $row->part_name. "</td>";
                                            echo "<td>" . $row->part_shares. "</td>";
                                            echo "<td>" . $row->part_phone. "</td>";
                                            echo "</tr>";
                                        }
                                        echo '</tbody>';
                                        echo "</table>";
                                        echo "</div>";
                                        echo '<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary" onclick="printDiv()">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';
                                            
                                    }
                                    else{
                                        echo '<div class="alert alert-danger alert-dismissible" role="alert" >
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <strong>no club owners have been registered</strong></div>';
                                    }
                                        ?>

                            


                                         
                                            
                                
                            
                            
                            
                
            </div>
    </div>
  