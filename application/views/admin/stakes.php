<div id="page-wrapper">
           <div class="row">
            <div class="panel panel-default">
            <div class="panel-heading">Clubs Shareholders</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($m_success)) ? $m_success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($stake_error)) ? $stake_error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/stake_reg'); ?>
                            
                                <form role="form" class="form form-horizontal">
                                <fieldset>
                                   <legend> Club Owner</legend>
                                    <div class="form-group col-sm-4 ">
                                    <label for="fname">Name</label>
                                    <input type="text" class="form-control"  name="own_name" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="lname">Shares(%)</label>
                                        <input type="text" class="form-control" name="own_shares" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 2)" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone Contact</label>
                                    <input type="text" class="form-control"  name="own_phone" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 15)" required>
                                </div>
                                </fieldset>
                                      <fieldset>
                                          <legend>Partners</legend>
                                          <div class="form-group col-sm-4 ">
                                    <label for="fname">Name</label>
                                    <input type="text" class="form-control"  name="part_name" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="lname">Shares(%)</label>
                                        <input type="text" class="form-control"  name="part_shares" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 2)" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone Contact</label>
                                    <input type="tel" class="form-control" name="part_phone" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 15)" required>
                                </div>
                                      </fieldset>                
                            
                             <!--     <div class="form-group col-sm-4 ">
                                    <label for="inputEmail">Club</label>                
                                     <select class="selectpicker form-control" name="club" required> 
                                          
                                            <?php
                                                #foreach($clubs as $each)
                                                {
                                                    ?>
                                                    
                                                    <?php
                                                }
                                                ?>
                                    </select>   
                                </div> -->
                               <!--  <div class="form-group col-sm-4 ">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" placeholder="Sikoli" name="password" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="c_password">Confirm Password</label>
                                    <input type="password" class="form-control" placeholder="Sikoli" name="c_password" required>
                                </div> -->
                               
                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-4">
                                        <button type="submit" class="btn btn-primary">SAVE
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                    </div>
                                </div>                                                          
                           </form>
                           
                           
                            <?php echo form_close();?>
                </div>
            </div>
    </div>
</div>
