<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/jquery-2.1.3.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
</head>
<body background="<?php echo base_url();?>images/futa.jpg">
	<div class="container" id="login">
		<div class="col-sm-4 col-sm-offset-3">	
			<div class="panel panel-default">
			  	<div class="panel-heading">Administrator Login</div>
			  	<div class="panel-body">
			  		<font color="red">
				  	<?php echo (isset($error)) ? $error : "" ?>
				  		<?php echo validation_errors(); ?>
				  	</font>       
			  		
			  		<?php echo form_open('a_ctlr/a_login'); ?>
			  		<form role="form">
					    <div class="form-group">
					      <label for="username">Username:</label>
					      <div class="input-group">
					      	<span class="glyphicon glyphicon-user input-group-addon" aria-hidden="true" ></span>
					      	<input type="text" class="form-control" id="email" placeholder="Enter username" name="username" required>
					      </div>					      
					    </div>
					    <div class="form-group">
					      <label for="pwd">Password:</label>
					      <div class="input-group">
					      	<span class="glyphicon glyphicon-lock input-group-addon" aria-hidden="true" ></span>
					      	 <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password" required>
					      </div>					     
					    </div>
					    <div class="form-group col-sm-6">
			  				<a href="<?=base_url('index')?>" class="btn btn-primary">
			  				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Not Here
			  				</a>
			  			</div>
					    <div class="form-group ">
					    	<button type="submit" class="btn btn-success col-sm-6">Login
					    	<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
					    	</button>
					    </div>					    
					 </form>
					 <?php echo form_close();?>
			  	</div>
		  </div>
		</div>  
	</div>

	   
</body>
</html>
<script src="assets/js/bootstrap.min.js"></script>
