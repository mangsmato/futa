 <script type="text/javascript">// <![CDATA[
function hide() {
    document.getElementById('hide_date').style.display = 'none';
}
function show() {
    document.getElementById('hide_date').style.display = 'block';
}

    </script>
<div id="page-wrapper" style="width: 80%; margin-left:1em;">
    <h1>Player Reports</h1>
          <div class="panel panel-default">
            <div class="panel-heading">Registered Players</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/report_players'); ?>
                             <form role="form" class="form-horizontal col-lg-12" >
                             
                                    <div class="form-group col-sm-3">
                                            <label for="inputEmail">All Players</label>
                                            <input type="radio" value="all" name="search" onclick="hide()" required>
                                           
                                        </div>
                                        <div class="form-group col-sm-4">
                                             <label for="inputEmail">Search by Club</label>
                                            <input type="radio" value="date" name="search" onclick="show()" required>
                                             <div id="hide_date" style="display:none;">
                                        <div class="form-group">
                                            <select name="club" class="form-control">
                                            <?php
                                                $query = $this->db->query("SELECT DISTINCT club FROM player");
                                                foreach ($query->result() as $row) {
                                                    echo '<option value="' . $row->club . '">' . $row->club . '</option>';
                                                }
                                            ?>
                                           </select>
                                            
                                        </div>
                                       
                                        </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary">Search
                                                        <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </div>
                                        </div>      
                                                                                           
                                    </form>
                                    <?php form_close();?> 
                        <div id="printable">
                            <?php
                            if (isset($players)) {
                                                            
                                if (is_array($players)) {
                                    echo '<div id="report_heading">';
                                    echo isset($heading) ? $heading : "" ;
                                    echo "</div>";
                                    // echo '<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    echo '<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Club Name</th>
                                          <th>Position</th>
                                          <th>Phone</th>
                                          <th>Email</th>
                                          <th>Nationality</th>
                                          <th>Age</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                        foreach ($players as $row ) {
                                            $no++;
                                            echo '<tr class="odd gradeX">';
                                            echo "<td>" . $no. "</td>";
                                             echo "<td>" . $row->fname . " " . $row->lname . "</td>";
                                            echo "<td>" . $row->club. "</td>";
                                            echo "<td>" . $row->position . "</td>";
                                            echo "<td>" . $row->phone. "</td>";
                                            echo "<td>" . $row->email. "</td>";
                                            echo "<td>" . $row->nationality. "</td>";
                                            echo "<td>" . $row->age. "</td>";
                                            echo "</tr>";
                                        }
                                        echo '</tbody>';
                                        echo "</table>";
                                        echo "</div>";
                                         // echo form_open('a_ctlr/c_pdf');
                                            echo ';<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary" onclick="printDiv()">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             // echo form_close();
                                }
                            }
                            ?>
                            
                </div>
            </div>
</div>
