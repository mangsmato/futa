<?php
$club_name=$stadium="";
    if (isset($edit_club)) {
         foreach ($edit_club as $row) {
            $club_name = $row->name;
            $stadium = $row->stadium;
         }
     }
?>
<div id="page-wrapper">
            <?php $tab = (isset($tab)) ? $tab : 'club_info'; ?>
            <ul class="nav nav-tabs">
                <li class="<?php echo ($tab == 'club_info') ? 'active':''; ?>">
                    <a href="#club_info" data-toggle="tab">CLUB DETAILS</a>
                </li>
                <li class="<?php echo ($tab == 'manager_info') ? 'active':''; ?>">
                    <a href="#manager_info" data-toggle="tab">MANAGER DETAILS</a>
                </li>
                <li class="<?php echo ($tab == 'stake_info') ? 'active':''; ?>">
                    <a href="#stake_info" data-toggle="tab">STAKE HOLDERS</a>
                </li>
            </ul>
                       <div class="tab-content">
                          <div class="tab-pane <?php echo ($tab == 'club_info') ? 'active' : ''; ?>" id="club_info">
                                    <div class="row col-sm-12">
                                <div class="panel panel-default">
                                <div class="panel-heading">Club Details</div>
                                    <div class="panel-body">
                                                <font color="green">
                                                    <?php echo (isset($success)) ? $success : "" ?>
                                                </font>
                                                <font color="red">
                                                    <?php echo (isset($error)) ? $error : "" ?>
                                                    <?php echo validation_errors(); ?>
                                                </font>       
                                                <?php echo form_open('a_ctlr/c_reg'); ?>
                                                 <form role="form" class="form form-horizontal">
                                                        <div class="form-group col-sm-4 ">
                                                                <label for="inputEmail">Club Name</label>
                                                                <input type="text" class="form-control" id="inputEmail"  name="club_name"  required>
                                                            </div>
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Home Stadium</label>
                                                                <input type="text" class="form-control" id="inputEmail"  name="stadium" required>
                                                            </div> 
                                                            <div class="form-group col-sm-4">
                                                                <label for="dob">Year of Formation</label>
                                                                <div class="input-group">
                                                                    <input id="dob" name="form_yr" type="text" class="form-control" required readonly format="yyyy-mm-dd">
                                                                    <span class="input-group-btn">
                                                                        <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('dob','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Address</label>
                                                                <input type="text" class="form-control" id="inputEmail" name="address"  required>
                                                            </div> 
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">City</label>
                                                                <input type="text" class="form-control" id="inputEmail"  name="city"  required>
                                                            </div> 
                                                            <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Club Email</label>
                                                                <input type="email" class="form-control" id="inputEmail"  name="email"  required>
                                                            </div> 
                                                             <div class="form-group col-sm-4">
                                                                <label for="inputEmail">Club Telephone</label>
                                                                <input type="tel" class="form-control" id="num"  name="phone"  onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 15)"  required>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-8 col-sm-4">
                                                                    <button type="submit" class="btn btn-primary" onclick="return checkLength()">NEXT
                                                                            <span class="glyphicon glyphicon-arrow-right"></span>
                                                                    </button>
                                                                </div>
                                                            </div> 
                                                                                                                     
                                                        </form>
                                                <?php form_close();?>
                                    </div>
                                </div>
                        </div>
                    </div>
                     <div class="tab-pane <?php echo ($tab == 'manager_info') ? 'active' : ''; ?>" id="manager_info">
                        <p><?php include 'add_manager.php'; ?></p>
                    </div>
                    <div class="tab-pane <?php echo ($tab == 'stake_info') ? 'active' : ''; ?>" id="stake_info">
                                    
                        <p><?php include 'stakes.php'; ?></p>
                    </div>
                    <!-- <div class="tab-pane <?php echo ($tab == 'guarantors') ? 'active' : ''; ?>" id="guarantors">
                                    
                        <p><?php #include 'guarantors.php'; ?></p>
                    </div> -->
                </div>
          </div>
    
