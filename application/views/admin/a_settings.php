<div id="page-wrapper">
    <h1>System Settings</h1>
        <div class="row col-sm-6">
            <div class="panel panel-default">
            <div class="panel-heading">Change Password</div>
                <div class="panel-body">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('a_ctlr/pass_change'); ?>
                            
                                <form>
                                <div class="form-group ">
                                    <label for="fname">Current Password</label>
                                    <input type="password" class="form-control"  name="c_pass" required>
                                </div>
                                <div class="form-group ">
                                    <label for="lname">New Password</label>
                                    <input type="password" class="form-control"  name="pass" required>
                                </div>
                                <div class="form-group ">
                                    <label for="uname">Confirm PAssword</label>
                                    <input type="password" class="form-control"   name="con_pass" required>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-4">
                                        <button type="submit" class="btn btn-primary">SAVE
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                    </div>
                                </div>                                                          
                           </form>
                           
                           
                            <?php form_close();?>
                </div>
            </div>
    </div>
</div>
