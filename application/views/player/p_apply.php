
    <script type="text/javascript" language="javascript">
        $(document).ready(function(){
            var totalChars      = 150; //Total characters allowed in textarea
            var countTextBox    = $('#reason') // Textarea input box
            var charsCountEl    = $('#countchars'); // Remaining chars count will be displayed here
           
            charsCountEl.text(totalChars); //initial value of countchars element
            countTextBox.keyup(function() { //user releases a key on the keyboard
               
                var thisChars = this.value.replace(/{.*}/g, '').length; //get chars count in textarea

                var per = thisChars *100;
                var value= (per / totalChars); // total percent complete
           
                if(thisChars > totalChars) //if we have more chars than it should be
                {
                    var CharsToDel = (thisChars-totalChars); // total extra chars to delete
                    this.value = this.value.substring(0,this.value.length-CharsToDel); //remove excess chars from textarea
                }else{
                    charsCountEl.text( totalChars - thisChars ); //count remaining chars
                    $('#percent').text(value +'%');
                }
            });
        });
    </script>
<div id="page-wrapper">
    <h1>Transfer Application</h1>
        <div class="row col-sm-8">
            <div class="panel panel-default">
            <div class="panel-heading">Apply for Transfer</div>
                <div class="panel-body">
                        <div class="ok_fade">
                            <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>   
                        </div>
                                
                            <?php echo form_open('p_ctlr/p_apply_save'); ?>
                                <div class="form-group">
                                    <label for="reason">Reason</label>
                                    <textarea class="form-control" name="reason" rows="6" id="reason" required>
                                        
                                    </textarea>
                                    <span name="countchars" id="countchars"></span> Characters Remaining
                                    <span name="percent" id="percent"></span>
                                </div>
                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary">SUBMIT APPLICATION
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                </div>                                                          
                           </form>
                           
                           
                            <?php form_close();?>
                </div>
            </div>
    </div>
</div>
