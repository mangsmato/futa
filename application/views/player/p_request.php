  <?php 
    $player = $this->session->userdata('player_sess');
        $player_id = $player['player_id'];
    $error="";


    
 ?>
<div id="page-wrapper" style="width:84%;margin-left:0.2em">
     <h1>Manager Negotiations</h1>
     
            <div class="panel panel-default">
            <div class="panel-heading">Replies from club interested in you</div>
                <div class="panel-body">
                  <div class="ok_fade">
                  <?php 
                  echo $this->session->flashdata('ok');
                  echo $this->session->flashdata('error');
                  ?>

                  </div>
                        <?php

                           $query = $this->db->query("SELECT negotiation.id,dest_club,buying_salary,m_reply
                            FROM negotiation  INNER JOIN  transfer ON negotiation.player_id=transfer.player_id 
                            WHERE negotiation.player_id ='$player_id' AND status='INCOMPLETE' AND
                            negotiation.player_id NOT IN (SELECT player_id FROM negotiation WHERE p_reply='REJECTED')");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover big_table" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Club Name</th>
                                          <th>Salary (Ksh.)</th>
                                          <th>Club Reply</th>
                                          <th>Your Reply</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->dest_club . "</td>";
                                        echo "<td>" . number_format($row->buying_salary,2) . "</td>";
                                        echo "<td>" . $row->m_reply . "</td>";
                                        echo '<td>'.
                                            anchor("p_ctlr/p_nego_ok/".$row->id,'<img src="' . base_url() . 'images/ok.png"/>',array('onclick' => "return confirm('Accept Request?')")). 
                                            anchor("p_ctlr/p_nego_edit/".$row->id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit Request?')")) .
                                             anchor("p_ctlr/p_nego_cancel/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Cancel Request?')")) .
                                            '</td>';
                                      
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                                // echo form_open('a_ctlr/c_pdf');
                                //             echo ';<div class="col-sm-offset-10 col-sm-4">
                                //                 <button type="submit" class="btn btn-primary">PRINT
                                //                      <span class="glyphicon glyphicon-print"></span>
                                //                 </button>
                                //             </div>';

                                //              echo form_close();
                            }
                            else{
                                $error = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>you have not received any application from clubs </strong></div>' ;
                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
    </div>
</div>

