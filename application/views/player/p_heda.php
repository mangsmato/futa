 <?php 
 	$player = $this->session->userdata('player_sess');
 	if (empty($player)) {
 		redirect('index');
 	}
 	$username = $player['username'];
 ?>                                                                                                                                                                                                                                                                <!DOCTYPE html>
<html>
<head>
  <title><?php echo (isset($title)) ? $title : "title not set";?></title>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
  <!-- <link href="<?php// echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> -->
  <meta charset = "UTF-8">


 
</head>
<body style="background:#FFF;">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#hor_lines">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <div class="navbar-brand">
	      	<img src="<?php echo base_url();?>images/futa.jpg">
	      	<div class="system">KFF Online Transfer System</div>
	      	
	      </div>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="hor_lines">
	      <ul class="nav navbar-nav navbar-right">
	        
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
	          	<i class="fa fa-university fa-fw"></i>
	          	Club
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="<?=base_url('p_apply');?>"><i class="fa fa-save fa-fw"></i>Apply Transfer</a></li>
	            <li><a href="<?=base_url('tr_reply');?>"><i class="fa fa-save fa-fw"></i>Transfer Reply</a></li>
	          </ul>
	        </li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
	          <i class="fa fa-user fa-fw"></i>
	          	Transfers 
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="<?=base_url('p_request');?>"><i class="fa fa-flag fa-fw"></i>Reply to Requests</a></li>
		        <li><a href="<?=base_url('p_req_report');?>"><i class="fa fa-inbox fa-fw"></i>View Requests</a></li>
	          </ul>
	        </li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo 'Hi ' . $username;?><span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="<?=base_url('p_settings');?>"><i class="fa fa-wrench fa-fw"></i>System Settings</a></li>
	            <li><a href="<?=base_url('logout');?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
	          </ul>
	        </li>
	      </ul>
	      
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container-fluid">
		<div id="heda-left">
			<div class="navbar-default sidebar" role="navigation">
		        <div class="sidebar-nav navbar-collapse">
		            <ul class="nav" id="side-menu">
			            <li>
			            	<label class="tree-toggler nav-header"><i class="fa fa-university fa-fw"></i>Club</label>
			            	<ul class="nav nav-list tree">
		                        <li><a href="<?=base_url('p_apply');?>"><i class="fa fa-save fa-fw"></i>Apply Transfer</a></li>
		                        <li><a href="<?=base_url('tr_reply');?>"><i class="fa fa-save fa-fw"></i>Transfer Reply</a></li>
		                    </ul>
			            </li>
			            <li>
			            	<label class="tree-toggler nav-header"><i class="fa fa-user fa-fw"></i>Transfers</label>
			            	<ul class="nav nav-list tree">
		                        <li><a href="<?=base_url('p_request');?>"><i class="fa fa-flag fa-fw"></i>Reply to Requests</a></li>
		                        <li><a href="<?=base_url('p_req_report');?>"><i class="fa fa-inbox fa-fw"></i>View Requests</a></li>
		                    </ul>
			            </li>
			             <li>
			            	<label class="tree-toggler nav-header"><i class="fa fa-laptop fa-fw"></i>System</label>
			            	<ul class="nav nav-list tree">
		                        <li><a href="<?=base_url('p_settings');?>"><i class="fa fa-wrench fa-fw"></i>Settings</a></li>	
		                        <li><a href="<?=base_url('contact')?>"><i class="fa fa-envelope fa-fw"></i>Contact Us</a></li>			            		
		                    </ul>
			            </li>
			        </ul>
			     </div>
			</div>
		</div>
		
  <script src="<?php echo base_url();?>assets/js/jquery-2.1.3.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			$('label.tree-toggler').click(function () {
				$(this).parent().children('ul.tree').toggle(300);
			});
		});

		// $('.selectpicker').selectpicker();
	</script>
	<script type="text/javascript">
    window.setTimeout(function() {
  $(".ok_fade").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 3000);

</script>
<script type="text/javascript">
    function printDiv() {
        var headstr = "<html><head><title>Club Owners</title></head><body>";
        var cont = "<center><h1> The Kenya Football Federation <br> <h5>your trusted football association</<h5></center></>"
     var printContents = document.getElementById('printable').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML =headstr + cont + printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
	
	
