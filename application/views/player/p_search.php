<div id="page-wrapper">
  <h1>Club Search</h1>
<?php echo $this->table->generate(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#big_table').DataTable( {
            "bProcessing": false,
            "bServerSide": false,
            "columns": [
                { "data": "name" },
                { "data": "stadium" },
                { "data": "" },
                { "data": "Action" }
              ],
            "sAjaxSource": "<?php echo base_url(); ?>p_ctlr/dataTable",
            "sServerMethod": "POST"
        } );
    } );
</script>
</div>

  
</body>
</html>
