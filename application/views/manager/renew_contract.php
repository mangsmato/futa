        <div id="page-wrapper">
            <div class="row">
                <div class="col-sm-8" >
                   <div class="panel panel-default">
                        <div class="panel-heading">Contract Renewal</div>

                        <div class="panel-body">
                            <div class="form-group col-sm-4">
                                <label for="inputEmail">Full Name</label>
                                 <p class="form-control-static">Mang'oli Martin</p>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="inputEmail">Remaining Duration</label>                
                                 <p class="form-control-static">0 years 2 Months</p>   
                             </div>
                             <div class="form-group col-sm-4">
                                <label for="inputEmail">Current Salary</label>
                                 <p class="form-control-static">$20000</p>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="inputEmail">New Salary</label>                
                                 <input type="number" class="form-control"  placeholder="Walikhe" name="fname" required min="1" max="10">
                             </div>
                             <div class="form-group col-sm-4">
                                <label for="inputEmail">Additional Years</label>                
                                 <input type="number" class="form-control"  placeholder="Walikhe" name="fname" required min="1" max="10">
                             </div>
                             <div class="form-group">
                                    <div class="col-sm-offset-10 col-sm-4">
                                        <button type="submit" class="btn btn-primary">SAVE
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                    </div>
                             </div>
                        </div>
                    </div>
                </div>
                    
            </div>
           
        </div>
        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  