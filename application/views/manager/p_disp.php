  <?php 
    $club_sess = $this->session->userdata('session_username');
 
    $club = $club_sess['club_name'];

    $error="";


    
 ?>

<div id="page-wrapper" style="width:80%;margin-left:1em">
     <h1><?php echo $club . " "; ?>Player Profiles</h1>
     
     <div class="ok_fade"><?php echo $this->session->flashdata('msg');?></div>
        <!-- <div class="row col-sm-12" > -->
            <div class="panel panel-default">
            <div class="panel-heading">your registered players</div>
            <br>
             
                        <?php

                           $query = $this->db->query("SELECT * FROM player INNER JOIN contract ON contract.player_id=player.player_id 
                            WHERE player.club='$club'");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Username</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Nationality</th>
                                          <th>Phone</th>
                                          <th>Salary(Ksh)</th>
                                          
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->player_id . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->nationality . "</td>";
                                         echo "<td>" . $row->phone . "</td>";
                                        echo "<td>" . number_format($row->salary,2) . "</td>";
                                        echo '<td>'.
                                            anchor("m_ctlr/p_prof_edit/".$row->id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit Profile?')")). 
                                            anchor("m_ctlr/p_prof_del/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Remove Player?')")) .
                                            '</td>';
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                              
                            }
                            else{
                                $error = '
                                <div class="alert alert-warning alert-dismissible col-sm-8" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  there are no players currently registered for your club' . anchor('p_reg', ' <strong>click here to register</strong>') .'</div>';

                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
            <!-- </div> -->
    </div>
</div>

