    <?php 
    $admin = $this->session->userdata('session_username');
    // if (empty($admin)) {
    //  redirect('index');
    // }
    $club = $admin['club_name'];
    $name=$id=$position=$salary=$club="";
    $value=$offer=0.00;
    if (isset($p_nego)) {
         foreach ($p_nego as $row) {
            $name = $row->fname . " " . $row->lname;
            $value = $row->selling_price;
            $id = $row->player_id;
            $position = $row->position;
            $offer = $row->buying_price;
            $club = $row->dest_club;
         }
     }
    
 ?>              

        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $club;?> Update Transfer Fee</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                    <div class="panel panel-default">
                       <div class="panel-heading">Please fill in the required details</div>
                       
                        <div class="panel-body">
                                
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        echo (isset($nego_error)) ? $nego_error: " ";
                                        ?>
                                    </font>
                                    <font color="green"><?php echo (isset($nego_success)) ? $nego_success: " "?></font>
                                    <?php echo form_open('m_ctlr/nego_edit_save'); ?>
                            
                                <form role="form" class="form">
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">PLAYER ID:</label>
                                         <p class="form-control-static"><?php echo $id; ?></p>
                                    </div>
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">NAME:</label>
                                        <p class="form-control-static"><?php echo $name; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">POSITION:</label>
                                        <p class="form-control-static"><?php echo $position; ?></p>
                                    </div>
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">INTERESTED CLUB</label>
                                        <p class="form-control-static"><?php echo $club; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">TRANSFER OFFER (Ksh.)</label>
                                        <p class="form-control-static"><?php echo number_format($offer,2); ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">YOUR OFFER (Ksh.)</label>
                                        <input type="number" min="1" class="form-control" id="inputEmail" placeholder="Mlima FC" name="release_close" value="<?php echo $value;?>" required>
                                    </div>
                                      
                                    <div class="form-group">
                                        <div class="col-sm-offset-8 col-sm-4">
                                         <a href="<?=base_url('nego')?>" type="button" class="btn btn-default">CANCEL</a>
                                            <button type="submit" class="btn btn-primary">SAVE
                                                <span class="glyphicon glyphicon-save"></span>
                                            </button>
                                    </div>
                                    </div> 
                                                                                              
                           </form>
                           <?php echo form_close();?>
                                             
                                </div>
                                                          
                        </div>
                    
            </div>

        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  