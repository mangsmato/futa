  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];

    $error="";


    
 ?>
<div id="page-wrapper" style="width:80%;margin-left:1em">
     <h1><?php echo $club . " "; ?>Buy Players</h1>
     
        <div class="row col-sm-12" >
            <div class="panel panel-default">
            <div class="panel-heading">Players available for buying</div>
                <div class="panel-body">
                  <div class="ok_fade">
                  <font color="green"><?php echo $this->session->flashdata('applied');?></font>
                  </div>
                        <?php

                           $query = $this->db->query("SELECT transfer.id,fname,lname,transfer_club,position,age,transfer_type,release_close 
                            FROM transfer JOIN player ON player.player_id=transfer.player_id
                            WHERE transfer_club!='$club' AND status='INCOMPLETE' AND 
                            transfer.player_id NOT IN (SELECT player_id FROM negotiation WHERE dest_club='$club')");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover big_table" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Club</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Transfer Type</th>
                                          <th>Transfer Fee(Ksh)</th>
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->transfer_club . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->transfer_type . "</td>";
                                        echo "<td>" . number_format($row->release_close,2) . "</td>";
                                        echo '<td>'.anchor("m_ctlr/p_buy_apply/".$row->id,'<button class="btn btn-primary">Apply</button>',array('onclick' => "return confirm('Do you want buy this player?')")) .'</td>';
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                                echo form_open('a_ctlr/c_pdf');
                                            echo ';<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             echo form_close();
                            }
                            else{
                                $error = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>there are no players currently on transfer from other clubs</strong>' ;
                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
            </div>
    </div>
</div>

