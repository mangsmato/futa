  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];

    $error="";


    
 ?>
<div id="page-wrapper" style="width:83%;margin-left:1em">
     <h1><?php echo $club . " "; ?>Transfer Applications</h1>
     
            <div class="panel panel-default">
            <div class="panel-heading">players you have requested to buy</div>
                <div class="panel-body">
                  <div class="ok_fade">
                  <?php 
                  echo $this->session->flashdata('ok');
                  echo $this->session->flashdata('error');
                  ?>

                  </div>
                        <?php

                           $query = $this->db->query("SELECT negotiation.id,fname,lname,source_club,position,age,transfer_type,selling_price,buying_price,s_reply 
                            FROM negotiation  INNER JOIN  transfer ON negotiation.player_id=transfer.player_id 
                            INNER JOIN player ON player.player_id=transfer.player_id
                            WHERE dest_club ='$club' AND level='MANAGERIAL' AND 
                            dest_club NOT IN (SELECT dest_club FROM negotiation WHERE b_reply='REJECTED')");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover big_table" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Club</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Transfer Type</th>
                                          <th>Transfer Fee(Ksh)</th>
                                          <th>Your Offer(Ksh)</th>
                                          <th>Club Reply</th>
                                          <th>Your Reply</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->source_club . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->transfer_type . "</td>";
                                        echo "<td>" . number_format($row->selling_price,2) . "</td>";
                                        echo "<td>" . number_format($row->buying_price,2) . "</td>";
                                        echo "<td>" . $row->s_reply . "</td>";
                                        echo '<td>'.
                                            anchor("m_ctlr/buy_ok/".$row->id,'<img src="' . base_url() . 'images/ok.png"/>',array('onclick' => "return confirm('Accept Request?')")). 
                                            anchor("m_ctlr/buy_edit/".$row->id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit Request?')")) .
                                             anchor("m_ctlr/buy_cancel/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Cancel Request?')")) .
                                            '</td>';
                                       
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                                echo form_open('a_ctlr/c_pdf');
                                            echo ';<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             echo form_close();
                            }
                            else{
                                $error = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>you have not applied for any player</strong></div>' ;
                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
    </div>
</div>

