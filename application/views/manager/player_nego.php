  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];

    $error="";


    
 ?>
<div id="page-wrapper" style="width:84%;margin-left:0.2em">
     <h1><?php echo $club . " "; ?>Player Negotiations</h1>
     
            <div class="panel panel-default">
            <div class="panel-heading">Replies from players you are interested in</div>
                <div class="panel-body">
                  <div class="ok_fade">
                  <?php 
                  echo $this->session->flashdata('ok');
                  echo $this->session->flashdata('error');
                  ?>

                  </div>
                        <?php

                           $query = $this->db->query("SELECT negotiation.id,fname,lname,source_club,position,age,transfer_type,selling_salary,buying_salary,p_reply 
                            FROM negotiation  INNER JOIN  transfer ON negotiation.player_id=transfer.player_id 
                            INNER JOIN player ON player.player_id=transfer.player_id
                            WHERE dest_club ='$club' AND level='PLAYER' AND dest_club 
                            NOT IN (SELECT dest_club FROM negotiation WHERE m_reply='REJECTED')");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover big_table" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Club</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Transfer Type</th>
                                          <th>Your Offer(Ksh)</th>
                                          <th>Requested Salary(Ksh)</th>
                                          <th>Player Reply</th>
                                          <th>Your Reply</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->source_club . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->transfer_type . "</td>";                                        
                                        echo "<td>" . number_format($row->buying_salary,2) . "</td>";
                                        echo "<td>" . number_format($row->selling_salary,2) . "</td>";
                                        echo "<td>" . $row->p_reply . "</td>";
                                         echo '<td>'.
                                            anchor("m_ctlr/m_nego_ok/".$row->id,'<img src="' . base_url() . 'images/ok.png"/>',array('onclick' => "return confirm('Accept Request?')")). 
                                            anchor("m_ctlr/m_nego_edit/".$row->id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit Request?')")) .
                                             anchor("m_ctlr/m_nego_cancel/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Cancel Request?')")) .
                                            '</td>';
                                        
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                                // echo form_open('a_ctlr/c_pdf');
                                //             echo ';<div class="col-sm-offset-10 col-sm-4">
                                //                 <button type="submit" class="btn btn-primary">PRINT
                                //                      <span class="glyphicon glyphicon-print"></span>
                                //                 </button>
                                //             </div>';

                                //              echo form_close();
                            }
                            else{
                                $error = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>you have not received any reply from any player </strong></div>' ;
                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
    </div>
</div>

