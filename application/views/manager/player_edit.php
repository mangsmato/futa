  <?php 
    $club_sess = $this->session->userdata('session_username');
 
    $club = $club_sess['club_name'];

    $error="";


    
 ?>

<div id="page-wrapper" style="width:80%;margin-left:1em">
     <h1><?php echo $club . " "; ?>Add to Transfer List</h1>
     
     
        <!-- <div class="row col-sm-12" > -->
            <div class="panel panel-default">
            <div class="panel-heading">Register Players to Transfer List</div>
            <br>
             
                        <?php

                           $query = $this->db->query("SELECT * FROM player INNER JOIN contract ON contract.player_id=player.player_id 
                            WHERE player.club='$club' AND player.player_id NOT IN (SELECT player_id FROM  transfer)");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Username</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Value (Ksh)</th>
                                          <th>Salary(Ksh)</th>
                                          <th>Contract End</th>
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->player_id . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . number_format($row->value,2) . "</td>";
                                        echo "<td>" . number_format($row->salary,2) . "</td>";
                                        echo "<td>" . $row->contract_end . "</td>";
                                        echo '<td>'.anchor("m_ctlr/p_get/".$row->id,'<button class="btn btn-primary">Transfer</button>',array('onclick' => "return confirm('Do you want transfer this player?')")) .'</td>';
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                              
                            }
                            else{
                                $error = '
                                <div class="alert alert-warning alert-dismissible col-sm-8" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  there are no players currently registered for your club' . anchor('p_reg', ' <strong>click here to register</strong>') .'</div>';

                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
            <!-- </div> -->
    </div>
</div>

