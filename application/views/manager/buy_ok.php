    <?php 
    $admin = $this->session->userdata('session_username');
    $club = $admin['club_name'];
    $name=$id=$position=$club="";
    $offer=$value=0.0;
    if (isset($b_ok)) {
         foreach ($b_ok as $row) {
            $name = $row->fname . " " . $row->lname;
            $value = $row->buying_price;
            $id = $row->player_id;
            $position = $row->position;
            $offer = $row->buying_salary;
            $club = $row->source_club;
         }
     }
    
 ?>              

        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $club;?> Offer Transfer Salary</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                    <div class="panel panel-default">
                       <div class="panel-heading">Please fill in the required details</div>
                       
                        <div class="panel-body">
                                
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        
                                        ?>
                                    </font>
                                    <?php 
                                    echo (isset($buy_error)) ? $buy_error: " ";
                                    echo (isset($buy_success)) ? $buy_success: " ";
                                    ?>
                                    <?php echo form_open('m_ctlr/buy_ok_save'); ?>
                            
                                <form role="form" class="form">
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">NAME:</label>
                                        <p class="form-control-static"><?php echo $name; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">POSITION:</label>
                                        <p class="form-control-static"><?php echo $position; ?></p>
                                    </div>
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">SOURCE CLUB</label>
                                        <p class="form-control-static"><?php echo $club; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">TRANSFER VALUE (Ksh.)</label>
                                        <p class="form-control-static"><?php echo  number_format($value,2); ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">YOUR SALARY OFFER  (Ksh.)</label>
                                        <input type="number" min="1" class="form-control" id="inputEmail" placeholder="Mlima FC" name="buying_salary" value="<?php echo $offer;?>" required>
                                    </div>
                                      
                                    <div class="form-group">
                                        <div class="col-sm-offset-8 col-sm-4">
                                            <a href="<?=base_url('apps')?>" type="button" class="btn btn-default">CANCEL</a>
                                            <button type="submit" class="btn btn-primary">SAVE
                                                <span class="glyphicon glyphicon-save"></span>
                                            </button>
                                    </div>
                                    </div> 
                                                                                              
                           </form>
                           <?php echo form_close();?>
                                             
                                </div>
                                                          
                        </div>
                    
            </div>

        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  