    <?php 

    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];
    $fname=$lname=$dob=$nationality=$value=$salary=$email=$previous_club=$position=$jazz_no=$jazz_name=$duration=$phone=$player_id="";
    if (isset($result)) {
        foreach ($result as $row) {
           $fname=$row->fname;
           $lname=$row->lname;
           $dob=$row->dob; 
           $position=$row->position;
           $nationality=$row->nationality;
           $value=$row->value;
           $salary=$row->salary;
           $email=$row->email;
           $jazz_no=$row->jazz_no;
           $jazz_name=$row->jazz_name;
           $duration=$row->duration;
           $phone=$row->phone;
           $previous_club=$row->previous_club;
           $player_id=$row->player_id;
        }
    }

        $countries = array("AF" => "Afghanistan","AX" => "Åland Islands","AL" => "Albania","DZ" => "Algeria","AS" => "American Samoa", "AD" => "Andorra",
    "AO" => "Angola","AI" => "Anguilla", "AQ" => "Antarctica","AG" => "Antigua and Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia",
    "AT" => "Austria","AZ" => "Azerbaijan",
    "BS" => "Bahamas",
    "BH" => "Bahrain",
    "BD" => "Bangladesh",
    "BB" => "Barbados",
    "BY" => "Belarus",
    "BE" => "Belgium",
    "BZ" => "Belize",
    "BJ" => "Benin",
    "BM" => "Bermuda",
    "BT" => "Bhutan",
    "BO" => "Bolivia",
    "BA" => "Bosnia and Herzegovina",
    "BW" => "Botswana",
    "BV" => "Bouvet Island",
    "BR" => "Brazil",
    "IO" => "British Indian Ocean Territory",
    "BN" => "Brunei Darussalam",
    "BG" => "Bulgaria",
    "BF" => "Burkina Faso",
    "BI" => "Burundi",
    "KH" => "Cambodia",
    "CM" => "Cameroon",
    "CA" => "Canada",
    "CV" => "Cape Verde",
    "KY" => "Cayman Islands",
    "CF" => "Central African Republic",
    "TD" => "Chad",
    "CL" => "Chile",
    "CN" => "China",
    "CX" => "Christmas Island",
    "CC" => "Cocos (Keeling) Islands",
    "CO" => "Colombia",
    "KM" => "Comoros",
    "CG" => "Congo",
    "CD" => "DRC",
    "CK" => "Cook Islands",
    "CR" => "Costa Rica",
    "CI" => "Cote D'ivoire",
    "HR" => "Croatia",
    "CU" => "Cuba",
    "CY" => "Cyprus",
    "CZ" => "Czech Republic",
    "DK" => "Denmark",
    "DJ" => "Djibouti",
    "DM" => "Dominica",
    "DO" => "Dominican Republic",
    "EC" => "Ecuador",
    "EG" => "Egypt",
    "SV" => "El Salvador",
    "GQ" => "Equatorial Guinea",
    "ER" => "Eritrea",
    "EE" => "Estonia",
    "ET" => "Ethiopia",
    "FK" => "Falkland Islands (Malvinas)",
    "FO" => "Faroe Islands",
    "FJ" => "Fiji",
    "FI" => "Finland",
    "FR" => "France",
    "GF" => "French Guiana",
    "PF" => "French Polynesia",
    "TF" => "French Southern Territories",
    "GA" => "Gabon",
    "GM" => "Gambia",
    "GE" => "Georgia",
    "DE" => "Germany",
    "GH" => "Ghana",
    "GI" => "Gibraltar",
    "GR" => "Greece",
    "GL" => "Greenland",
    "GD" => "Grenada",
    "GP" => "Guadeloupe",
    "GU" => "Guam",
    "GT" => "Guatemala",
    "GG" => "Guernsey",
    "GN" => "Guinea",
    "GW" => "Guinea-bissau",
    "GY" => "Guyana",
    "HT" => "Haiti",
    "HM" => "Heard Island and Mcdonald Islands",
    "VA" => "Holy See (Vatican City State)",
    "HN" => "Honduras",
    "HK" => "Hong Kong",
    "HU" => "Hungary",
    "IS" => "Iceland",
    "IN" => "India",
    "ID" => "Indonesia",
    "IR" => "Iran",
    "IQ" => "Iraq",
    "IE" => "Ireland",
    "IM" => "Isle of Man",
    "IL" => "Israel",
    "IT" => "Italy",
    "JM" => "Jamaica",
    "JP" => "Japan",
    "JE" => "Jersey",
    "JO" => "Jordan",
    "KZ" => "Kazakhstan",
    "KE" => "Kenya",
    "KI" => "Kiribati",
    "KP" => "Korea",
    "KR" => "Korea, Republic of",
    "KW" => "Kuwait",
    "KG" => "Kyrgyzstan",
    "LA" => "Lao People's Democratic Republic",
    "LV" => "Latvia",
    "LB" => "Lebanon",
    "LS" => "Lesotho",
    "LR" => "Liberia",
    "LY" => "Libyan Arab Jamahiriya",
    "LI" => "Liechtenstein",
    "LT" => "Lithuania",
    "LU" => "Luxembourg",
    "MO" => "Macao",
    "MK" => "Macedonia",
    "MG" => "Madagascar",
    "MW" => "Malawi",
    "MY" => "Malaysia",
    "MV" => "Maldives",
    "ML" => "Mali",
    "MT" => "Malta",
    "MH" => "Marshall Islands",
    "MQ" => "Martinique",
    "MR" => "Mauritania",
    "MU" => "Mauritius",
    "YT" => "Mayotte",
    "MX" => "Mexico",
    "FM" => "Micronesia",
    "MD" => "Moldova",
    "MC" => "Monaco",
    "MN" => "Mongolia",
    "ME" => "Montenegro",
    "MS" => "Montserrat",
    "MA" => "Morocco",
    "MZ" => "Mozambique",
    "MM" => "Myanmar",
    "NA" => "Namibia",
    "NR" => "Nauru",
    "NP" => "Nepal",
    "NL" => "Netherlands",
    "AN" => "Netherlands Antilles",
    "NC" => "New Caledonia",
    "NZ" => "New Zealand",
    "NI" => "Nicaragua",
    "NE" => "Niger",
    "NG" => "Nigeria",
    "NU" => "Niue",
    "NF" => "Norfolk Island",
    "MP" => "Northern Mariana Islands",
    "NO" => "Norway",
    "OM" => "Oman",
    "PK" => "Pakistan",
    "PW" => "Palau",
    "PS" => "Palestinian Territory",
    "PA" => "Panama",
    "PG" => "Papua New Guinea",
    "PY" => "Paraguay",
    "PE" => "Peru",
    "PH" => "Philippines",
    "PN" => "Pitcairn",
    "PL" => "Poland",
    "PT" => "Portugal",
    "PR" => "Puerto Rico",
    "QA" => "Qatar",
    "RE" => "Reunion",
    "RO" => "Romania",
    "RU" => "Russian Federation",
    "RW" => "Rwanda",
    "SH" => "Saint Helena",
    "KN" => "Saint Kitts and Nevis",
    "LC" => "Saint Lucia",
    "PM" => "Saint Pierre and Miquelon",
    "VC" => "Saint Vincent and The Grenadines",
    "WS" => "Samoa",
    "SM" => "San Marino",
    "ST" => "Sao Tome and Principe",
    "SA" => "Saudi Arabia",
    "SN" => "Senegal",
    "RS" => "Serbia",
    "SC" => "Seychelles",
    "SL" => "Sierra Leone",
    "SG" => "Singapore",
    "SK" => "Slovakia",
    "SI" => "Slovenia",
    "SB" => "Solomon Islands",
    "SO" => "Somalia",
    "ZA" => "South Africa",
    "GS" => "South Georgia and The South Sandwich Islands",
    "ES" => "Spain",
    "LK" => "Sri Lanka",
    "SD" => "Sudan",
    "SR" => "Suriname",
    "SJ" => "Svalbard and Jan Mayen",
    "SZ" => "Swaziland",
    "SE" => "Sweden",
    "CH" => "Switzerland",
    "SY" => "Syrian Arab Republic",
    "TW" => "Taiwan, Province of China",
    "TJ" => "Tajikistan",
    "TZ" => "Tanzania",
    "TH" => "Thailand",
    "TL" => "Timor-leste",
    "TG" => "Togo",
    "TK" => "Tokelau",
    "TO" => "Tonga",
    "TT" => "Trinidad and Tobago",
    "TN" => "Tunisia",
    "TR" => "Turkey",
    "TM" => "Turkmenistan",
    "TC" => "Turks and Caicos Islands",
    "TV" => "Tuvalu",
    "UG" => "Uganda",
    "UA" => "Ukraine",
    "AE" => "United Arab Emirates",
    "GB" => "United Kingdom",
    "US" => "United States",
    "UM" => "United States Minor Outlying Islands",
    "UY" => "Uruguay",
    "UZ" => "Uzbekistan",
    "VU" => "Vanuatu",
    "VE" => "Venezuela",
    "VN" => "Viet Nam",
    "VG" => "Virgin Islands, British",
    "VI" => "Virgin Islands, U.S.",
    "WF" => "Wallis and Futuna",
    "EH" => "Western Sahara",
    "YE" => "Yemen",
    "ZM" => "Zambia",
    "ZW" => "Zimbabwe");

    $prev_club = array(
        '1' => 'AFC Leopards' , '2' => 'Bandari','3' => 'Chemelil Sugar', '4' => 'Gor Mahia', '5' => 'KCB',
        '6' => 'KRA','7' => 'Mathare United','8' => 'Muhoroni Youth','9' => 'Nairobi City Stars','10' => 'Nakuru All Stars','11' => 'Sofapaka ',
        '12' => 'Sony Sugar','13' => 'Thika United','14' => 'Tusker FC','15' => 'Ulinzi Stars','16' => 'Western Stima'
        );

    
 ?>              
        
        <div id="page-wrapper" style="margin-top:-4em;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $club;?> Player Update</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                    <div class="panel panel-default">
                       <div class="panel-heading">Please fill in the required details</div>
                       
                        <div class="panel-body">
                                
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        echo (isset($p_reg_error)) ? $p_reg_error: " ";
                                        ?>
                                    </font>
                                    <font color="green"><?php echo (isset($p_reg_success)) ? $p_reg_success: " "?></font>
                                    <?php echo form_open('m_ctlr/p_prof_update'); ?>
                            
                                <form role="form" class="form form-horizontal">
                                <fieldset>
                                    <legend>Personal Details <?php echo " ". $player_id?></legend>
                                    <div class="form-group col-sm-4 ">
                                    <label for="fname">First Name</label>
                                    <input type="text" class="form-control" value="<?php echo  $fname;?>"  name="fname" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="lname">Last Name</label>
                                    <input type="text" class="form-control" value="<?php echo $lname;?>"  name="lname" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="uname">Nationality: <?php echo $nationality;?></label>
                                    <select class="form-control"   name="nationality" required>
                                        <?php
                                        foreach($countries as $key => $country) {
                                            echo '<option value="' . $country . '">' . $country . '</option>' ;
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="dob">Date of Birth</label>
                                    <div class="input-group">
                                        <input id="dob" name="dob" type="text" value="<?php echo $dob;?>"  class="form-control" required readonly format="yyyy-mm-dd">
                                        <span class="input-group-btn">
                                            <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('dob','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone</label>
                                    <input type="tel" class="form-control" value="<?php echo $phone;?>"   name="phone" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 15)"  required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="phone">Email Address</label>
                                    <input type="email" class="form-control"  name="email" value="<?php echo $email;?>" >
                                </div>
                                 <div class="form-group col-sm-4 ">
                                    <label for="inputEmail">Position: <?php echo $position;?></label>                
                                     <select class="selectpicker form-control" name="position" required> 
                                            <option value="1">1</option>  
                                            <option value="2">2</option>   
                                            <option value="3">3</option> 
                                            <option value="4">4</option> 
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>  
                                            <option value="8">8</option>   
                                            <option value="9">9</option> 
                                            <option value="10">10</option> 
                                            <option value="11">11</option>
                                    </select>   
                                </div>
                               <div class="form-group col-sm-4 ">
                                    <label for="value">Value (Ksh.)</label>
                                    <input type="number" class="form-control"  name="value" value="<?php echo $value;?>"  id="value" required min="20000">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="salary">Salary (Ksh.)</label>
                                    <input type="number" class="form-control" name="salary" value="<?php echo $salary;?>"  id="salary" required min="20000">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="duration">Contract Duration(Years)</label>
                                    <input type="number" class="form-control"  name="duration" value="<?php echo $duration;?>"  required min="3" max="5" id="duration">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="inputEmail">Previous Club: <?php echo $previous_club;?> </label>                
                                     <select class="form-control"   name="p_club" required>
                                     <option value="None">None</option>
                                        <?php
                                        foreach($prev_club as $key => $c) {
                                            echo '<option value="' . $c . '">' . $c . '</option>' ;
                                        }
                                        ?>
                                    </select>
                                </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Account Details</legend>
                                    <div class="form-group col-sm-4 ">
                                        <label for="jazz_name">Jersey Name</label>
                                        <input type="text" class="form-control"   name="jazz_name" value="<?php echo $jazz_name;?>"  required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="jazz_number">Jersey Number</label>
                                        <input type="number" class="form-control" name="jazz_no" value="<?php echo $jazz_no;?>"  required min="1">
                                    </div>
                                    <!-- <div class="form-group col-sm-4 ">
                                        <label for="passsword">Password</label>
                                        <input type="password" class="form-control"  placeholder="Walikhe" name="password" required>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="-Cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" placeholder="Sikoli" name="c_password" required>
                                    </div> -->
                                </fieldset>
                                
                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-4">
                                    <a href="<?=base_url('players_prof')?>" type="button" class="btn btn-default">CANCEL</a>
                                        <button type="submit" class="btn btn-primary">SAVE
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                    </div>
                                </div>                                                          
                           </form>
                                                              
                                </div>
                                                          
                        </div>
                    
            </div>

        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  