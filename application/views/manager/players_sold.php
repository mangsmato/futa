  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];
    $error="";


    
 ?>
<div id="page-wrapper" style="width:82%;margin-left:1em">
<div id="printable">
     <h1><?php echo $club . " "; ?>Players Sold</h1>
     
   <!--      <div class="row col-sm-12" >
            <div class="panel panel-default">
            <div class="panel-heading">Players Bought</div>
                <div class="panel-body"> -->
                        <?php
                           $query = $this->db->query("SELECT * FROM transfer
                                    JOIN transfer_listed ON transfer.player_id=transfer_listed.player_id
                                    JOIN negotiation ON transfer.player_id=negotiation.player_id WHERE source_club='$club' AND status='COMPLETE'");
                            if ($query->num_rows()>0) {
                                // echo '<table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                              echo '<table  class="table table-striped table-bordered table-hover" >
                                        <thead>
                                          <th>No.</th>
                                          <th>Name</th>
                                          <th>Age</th>
                                          <th>Position</th>
                                          <th>Destination Club</th>
                                          <th>Transfer Type</th>
                                          <th>Transfer Amount(Ksh)</th>
                                          <th>Date Completed</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->dest_club . "</td>";
                                        echo "<td>" . $row->transfer_type . "</td>";
                                        echo "<td>" . number_format($row->buying_price,2) . "</td>";
                                        echo "<td>" . $row->date_completed . "</td>";
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '</table>';
                                echo "</div>";
                                // echo form_open('a_ctlr/c_pdf');
                                            echo ';<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary" onclick="printDiv()">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             // echo form_close();
                            }
                            else{
                                $error = "you have not sold any player " . anchor('p_edit', 'Click Here') . " to add to transfer list";
                            }
                            ?> 
                            <div class="ok_fade">
                             <font color="green"><?php echo $error ?></font>
                           </div>
       <!--          </div>
            </div>
    </div> -->
</div>

