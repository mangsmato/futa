    <?php 
    $admin = $this->session->userdata('session_username');
    // if (empty($admin)) {
    //  redirect('index');
    // }
    $club = $admin['club_name'];
    $name=$value=$id=$position=$salary=$offer=$club="";
    if (isset($p_list_edit)) {
         foreach ($p_list_edit as $row) {
            $name = $row->fname . " " . $row->lname;
            $value = $row->release_close;
            $id = $row->player_id;
            $position = $row->position;
         }
     }
    
 ?>   
         

        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $club;?> Update Transfer List</h1>
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
           
            <div class="row">
                    <div class="panel panel-default">
                       <div class="panel-heading">Please fill in the required details</div>
                       
                        <div class="panel-body">
                                
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        echo (isset($list_error)) ? $nego_error: " ";
                                        ?>
                                    </font>
                                    <font color="green"><?php echo (isset($list_success)) ? $list_success: " "?></font>
                                    <?php echo form_open('m_ctlr/list_edit_save'); ?>
                            
                                <form role="form" class="form">
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">PLAYER ID:</label>
                                         <p class="form-control-static"><?php echo $id; ?></p>
                                    </div>
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">NAME:</label>
                                        <p class="form-control-static"><?php echo $name; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">POSITION:</label>
                                        <p class="form-control-static"><?php echo $position; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">TRANSFER TYPE:</label>
                                        <select name="transfer_type" class="form-control">
                                            <option value="Sale">Sale</option>
                                            <option value="Loan">Loan</option>
                                            <option value="Free">Free</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">Transfer Fee</label>
                                        <input type="number" min="0" class="form-control" id="inputEmail" placeholder="Mlima FC" name="release_close" value="<?php echo $value;?>" required>
                                    </div>
                                      
                                    <div class="form-group">
                                        <div class="col-sm-offset-8 col-sm-4">
                                            <a href="<?=base_url('tr_list')?>" type="button" class="btn btn-default">CANCEL</a>
                                            <button type="submit" class="btn btn-primary">SAVE
                                                <span class="glyphicon glyphicon-save"></span>
                                            </button>
                                    </div>
                                    </div> 
                                                                                              
                           </form>
                           <?php echo form_close();?>
                                             
                                </div>
                                                          
                        </div>
                    
            </div>

        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  