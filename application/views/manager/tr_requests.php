  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];

    $error="";


    
 ?>
<script type="text/javascript">
function con(message) {
 var answer = confirm(message);
 if (answer) {
  return true;
 }

 return false;
}
</script>
<div id="page-wrapper" style="width:80%;margin-left:0.2em">
     <h1><?php echo $club . " "; ?> Transfer Requests</h1>
     <div class="ok_fade">
     <?php 
       echo $this->session->flashdata('ok');
        echo  $this->session->flashdata('error');
     ?></div>
        <div class="row col-sm-12" >
            <div class="panel panel-default">
            <div class="panel-heading">Applied transfer requests from club players</div>
                <div class="panel-body">
                   <?php

                           $query = $this->db->query("SELECT transfer_request.id,fname,lname,position,age,contract_end,reason 
                            FROM player INNER JOIN contract ON contract.player_id=player.player_id 
                            INNER JOIN transfer_request ON transfer_request.player_id=player.player_id
                            WHERE player.club='$club' AND reply IS NULL");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Contract End</th>
                                          <th width="200">Reason</th>
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->contract_end . "</td>";
                                        echo "<td>" . $row->reason . "</td>";
                                        echo '<td>'.
                                            anchor("m_ctlr/m_req_ok/".$row->id,'<img src="' . base_url() . 'images/ok.png"/>',array('onclick' => "return confirm('Accept Request?')")). 
                                            anchor("m_ctlr/m_req_reject/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Reject Request?')")) .
                                            '</td>';
                                       
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                              
                            }
                            else{
                                $error = '
                                <div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>there are no requests from your players </strong></div>';

                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
            </div>
    </div>
</div>

