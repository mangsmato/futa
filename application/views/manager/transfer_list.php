  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];

    $error="";


    
 ?>
<script type="text/javascript">
function con(message) {
 var answer = confirm(message);
 if (answer) {
  return true;
 }

 return false;
}
</script>
<div id="page-wrapper" style="width:80%;margin-left:1em">
     <h1><?php echo $club . " "; ?> Transfer List</h1>
     <div class="ok_fade">
     <?php 
       echo $this->session->flashdata('ok');
        echo  $this->session->flashdata('error');
     ?></div>
            <div class="panel panel-default">
            <div class="panel-heading">players on the transfer list</div>
                <div class="panel-body">
                   <?php

                           $query = $this->db->query("SELECT transfer.id,fname,lname,position,age,transfer_type,release_close
                            FROM player INNER JOIN transfer ON player.player_id=transfer.player_id 
                            WHERE player.club='$club' AND status='INCOMPLETE'");
                            if ($query->num_rows()>0) {
                                echo '<table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                          <th>No.</th>
                                          <th>Player Name</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Transfer Type</th>
                                          <th>Transfer Fee</th>
                                          <th>Action</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . $row->transfer_type . "</td>";
                                        echo "<td>" . $row->release_close . "</td>";
                                        echo '<td>'.
                                            anchor("m_ctlr/list_edit/".$row->id,'<img src="' . base_url() . 'images/edit.png"/>',array('onclick' => "return confirm('Edit this transfer?')")). 
                                            anchor("m_ctlr/list_remove/".$row->id,'<img src="' . base_url() . 'images/delete.png"/>',array('onclick' => "return confirm('Remove from list?')")) .
                                            '</td>';
                                       
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '<table>';
                              
                            }
                            else{
                                $error = '
                                <div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>you have no players on the transfer list' .  anchor('p_edit', ' <strong>click here to add</strong>') .'</div>';

                            }
                            ?> 
                            <div class="ok_fade">
                             <?php echo $error ?>
                           </div>
                </div>
    </div>
</div>

