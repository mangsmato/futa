  <?php 
    $admin = $this->session->userdata('session_username');
 
    $club = $admin['club_name'];
    $error="";


    
 ?>
<div id="page-wrapper" style="width:84%;margin-left:0.5em">
<div id="printable">
     <h1><?php echo $club . " "; ?>Players</h1>
     <div class="ok_fade">
         <font color="green"><?php echo $error ?></font>
     </div>
        
          <!--   <div class="panel panel-default" style="width:100%;">
            <div class="panel-heading">Registered Players</div>
                <div class="panel-body"> -->
                        <?php
                           $query = $this->db->query("SELECT * FROM player INNER JOIN contract ON player.player_id=contract.player_id WHERE player.club='$club'");
                            if ($query->num_rows()>0) {
                                // echo '<table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                              echo '<table  class="table table-striped table-bordered table-hover" >
                                        <thead>
                                          <th>No.</th>
                                          <th>Name</th>
                                          <th>Phone</th>
                                          <th>Position</th>
                                          <th>Age</th>
                                          <th>Value(Ksh.)</th>
                                          <th>Previous Club</th>
                                          <th>Nationality</th>
                                          <th>Contract Start</th>
                                          <th>Salary(Ksh)</th>
                                          <th>Contract End</th>
                                        </thead>';
                                        echo '<tbody>';
                                        $no=0;
                                foreach ($query->result() as $row) {
                                    $no++;
                                    echo "<tr>";
                                        echo "<td>" . $no . "</td>";
                                        echo "<td>" . $row->lname . " " . $row->fname . "</td>";
                                        echo "<td>" . $row->phone . "</td>";
                                        echo "<td>" . $row->position . "</td>";
                                        echo "<td>" . $row->age . "</td>";
                                        echo "<td>" . number_format($row->value,2) . "</td>";
                                        echo "<td>" . $row->previous_club . "</td>";
                                        echo "<td>" . $row->nationality . "</td>";
                                        echo "<td>" . $row->contract_start . "</td>";
                                        echo "<td>" . number_format($row->salary,2) . "</td>";
                                        echo "<td>" . $row->contract_end . "</td>";
                                    echo "</tr>";
                                    
                                }
                                echo '</tbody>';
                                echo '</table>';
                                echo '</div>';
                                // echo form_open('a_ctlr/c_pdf');
                                            echo '<div class="col-sm-offset-10 col-sm-4">
                                                <button type="submit" class="btn btn-primary" onclick="printDiv()">PRINT
                                                     <span class="glyphicon glyphicon-print"></span>
                                                </button>
                                            </div>';

                                             // echo form_close();
                            }
                            else{
                                $error = "you have not registered any player " . anchor('p_reg', 'Click Here') . " to register";
                            }
                            ?> 
       <!--          </div>
            </div>
 
</div> -->
</div>

