    <?php 
    $admin = $this->session->userdata('session_username');
    $club = $admin['club_name'];

    $nego_sess = $this->session->userdata('nego_reg');
    $nego_id = $nego_sess['nego_id'];

    $fname=$lname=$nationality=$dob=$phone=$position=$value=$salary=$p_club=$jazz_name=$jazz_no=$email="";

    $this->db->select('*');
    $this->db->from('negotiation');
    $this->db->join('player','player.player_id=negotiation.player_id');
    $this->db->where('negotiation.id',$nego_id);
    $query = $this->db->get();
    if ($query->num_rows()>0) {
        foreach ($query->result() as $row) {
                $fname = $row->fname;
                $lname = $row->lname;
                $nationality = $row->nationality;
                $dob = $row->dob;
                $phone = $row->phone;
                $email = $row->email;
                $position = $row->position;
                $value = $row->buying_price;
                $salary = $row->buying_salary;
                $p_club = $row->source_club;
                $jazz_name = $row->jazz_name;
                $jazz_no = $row->jazz_no;
                $player_id = $row->player_id;
            }
            // $trans_player = array('player_id' => $player_id);
            $this->session->set_userdata('p_update_sess', $player_id);



    }
    
 ?>              
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $club;?> Player Registration</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                    <div class="panel panel-default">
                       <div class="panel-heading">Please fill in the required details</div>
                       
                        <div class="panel-body">
                                
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        echo (isset($p_reg_error)) ? $p_reg_error: " ";
                                        ?>
                                    </font>
                                    <font color="green"><?php echo (isset($p_reg_success)) ? $p_reg_success: " "?></font>
                                    <?php echo form_open('m_ctlr/p_trans_save'); ?>
                            
                                <form role="form" class="form form-horizontal">
                                <fieldset>
                                    <legend>Personal Details</legend>
                                    <div class="form-group col-sm-4 ">
                                    <label for="fname">First Name</label>
                                    <input type="text" class="form-control"  placeholder="Walikhe" name="fname" value="<?php echo $fname;?>" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="lname">Last Name</label>
                                    <input type="text" class="form-control" placeholder="Sikoli" name="lname" value="<?php echo $lname;?>" required>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="uname">Nationality</label>
                                    <input type="text" class="form-control"  placeholder="Walikhe" name="nationality" value="<?php echo $nationality;?>" required>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="dob">Date of Birth</label>
                                    <div class="input-group">
                                        <input id="dob" name="dob" type="text" class="form-control" required readonly  value="<?php echo $dob;?>">
                                        <span class="input-group-btn">
                                            <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('dob','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="phone">Phone</label>
                                    <input type="tel" class="form-control" placeholder="Sikoli" name="phone" required value="<?php echo $phone;?>">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="phone">Email Address</label>
                                    <input type="email" class="form-control" placeholder="som@ex.com" name="email" value="<?php echo $email;?>" required>
                                </div>
                                 <div class="form-group col-sm-4 ">
                                    <label for="inputEmail">Position</label>                
                                     <input type="number" class="form-control" placeholder="50000" name="position" id="value" required min="1" value="<?php echo $position;?>"> 
                                </div>
                               <div class="form-group col-sm-4 ">
                                    <label for="value">Value</label>
                                    <input type="number" class="form-control" placeholder="50000" name="value" id="value" required min="100" value="<?php echo $value;?>">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="salary">Salary</label>
                                    <input type="number" class="form-control" placeholder="50000" name="salary" id="salary" required min="100" value="<?php echo $salary;?>">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="duration">Contract Duration(Years)</label>
                                    <input type="number" class="form-control" placeholder="3" name="duration" required min="3" max="5" id="duration">
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label for="inputEmail">Previous Club</label>                
                                    <input type="text" class="form-control"  placeholder="Walikhe" name="p_club" required  value="<?php echo $p_club;?>">  
                                </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Account Details</legend>
                                    <div class="form-group col-sm-4 ">
                                        <label for="jazz_name">Jersey Name</label>
                                        <input type="text" class="form-control"  placeholder="Walikhe" name="jazz_name" required value="<?php echo $jazz_name;?>">
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="jazz_number">Jersey Number</label>
                                        <input type="number" class="form-control" placeholder="9" name="jazz_no" required min="1" value="<?php echo $jazz_no;?>">
                                    </div>
                                  
                                </fieldset>
                                
                                <div class="form-group">
                                    <div class="col-sm-offset-10 col-sm-4">
                                        <button type="submit" class="btn btn-primary">SAVE
                                            <span class="glyphicon glyphicon-save"></span>
                                        </button>
                                    </div>
                                </div>                                                          
                           </form>
                           
                           
                            <?php form_close();?>
                                    
                                    <?php echo form_open('p_reg'); ?>
                                    <form>
                                        <div class="form-group">
                                            <div class=" col-sm-4">
                                                <button type="submit" class="btn btn-primary">NEW PLAYER
                                                    <span class="glyphicon glyphicon-open"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <?php echo form_close();?>
                                    
                                </div>
                                                          
                        </div>
                    
            </div>

        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  