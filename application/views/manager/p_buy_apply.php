    <?php 
    $admin = $this->session->userdata('session_username');
    // if (empty($admin)) {
    //  redirect('index');
    // }
    $club = $admin['club_name'];
    $name=$id=$position=$salary=$source_club=$type="";
    $value=0.0;
    $show_offer=false;
    if (isset($p_buy)) {
         foreach ($p_buy as $row) {
            $name = $row->fname . " " . $row->lname;
            $value = $row->release_close;
            $id = $row->player_id;
            $position = $row->position;
            $source_club = $row->transfer_club;
            $type=$row->transfer_type;
         }
     //     if ($type =="free") {
     //     echo '<style type="text/css">
     //          #hide{
     //                 display:none;
     //             }
     //         </style>';
     // }
    
     
     }
    
 ?>    
  
       

        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $club;?> Buy Player</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                    <div class="panel panel-default">
                       <div class="panel-heading">Please fill in the required details</div>
                       
                        <div class="panel-body">
                                
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        echo (isset($buy_error)) ? $buy_error: " ";
                                        ?>
                                    </font>
                                    <?php echo (isset($another)) ? $another: " ";?>
                                    <?php echo form_open('m_ctlr/p_update_transfer'); ?>
                            
                                <form role="form" class="form">
                                <div class="form-group col-sm-4 ">
                                        <label class="control-label">SELLING CLUB:</label>
                                         <p class="form-control-static"><?php echo $source_club; ?></p>
                                    </div>
                                     <div class="form-group col-sm-4 ">
                                        <label class="control-label">PLAYER NAME:</label>
                                        <p class="form-control-static"><?php echo $name; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label class="control-label">POSITION:</label>
                                        <p class="form-control-static"><?php echo $position; ?></p>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">TRANSFER TYPE</label>
                                        <p class="form-control-static"><?php echo $type;?>
                                    </div>
                                    <div class="form-group col-sm-4 ">
                                        <label for="inputEmail">TRANSFER FEE (Ksh.)</label>
                                        <p class="form-control-static"><?php echo number_format($value,2);?>
                                    </div>
                                      <div class="form-group col-sm-4 " id="hide">
                                        <label for="inputEmail">YOUR OFFER</label>
                                        <input type="number" min="0" class="form-control" id="inputEmail" placeholder="Mlima FC" name="buying_price"  required>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-8 col-sm-4">
                                        
                                            <a href="<?=base_url('p_edit')?>" type="button" class="btn btn-default">CANCEL</a>
                                            <button type="submit" class="btn btn-primary">SAVE
                                                <span class="glyphicon glyphicon-save"></span>
                                            </button>
                                    </div>
                                    </div> 
                                                                                              
                           </form>
                           <?php echo form_close();?>
                                             
                                </div>
                                                          
                        </div>
                    
            </div>

        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  