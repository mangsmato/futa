<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class M_ctlr extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
		}
		public function index()
		{
			$this->load->view('manager/coach');
		}
			
		public function m_login(){
			$this->form_validation->set_rules('username','Username','xss_clean|trim|required');
			$this->form_validation->set_rules('password','Password','xss_clean|trim|required');
			if ($this->form_validation->run() == FALSE) {
				// $this->m();
				$this->load->view('manager/coach');
			}
			else{
				$username=$this->input->post('username');
				$password=md5($this->input->post('password'));
				$result=$this->m_model->m_login($username,$password);
				if ($result) {
					redirect('p_reg');
				}
				else{
							
					$data["error"]='<div class="alert alert-danger alert-dismissible" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>incorrect username or password</strong></div><br><br><br><br>';
					$this->load->view('manager/coach',$data);
					
				}
			}
			
		}
		function player(){
	        $data['title'] = "Register Players";
	        $this->load->view('manager/m_heda',$data);
	        $this->load->view('manager/add_player',$data);
	    }
	    function p_trans_reg(){
	        $data['title'] = "Register Players";
	        $this->load->view('manager/m_heda',$data);
	        $this->load->view('manager/trans_player_reg',$data);
	    }
	    function p_save(){
	    	$this->form_validation->set_rules('fname','First Name','xss_clean|trim|required');
			$this->form_validation->set_rules('lname','Last Name','xss_clean|trim|required');
			$this->form_validation->set_rules('phone','Phone Number','xss_clean|trim|required|is_numeric|is_unique[player.phone]|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('nationality','Nationality','xss_clean|trim|required|');
			$this->form_validation->set_rules('email','Email Address','xss_clean|trim|valid_email|is_unique[player.email]');
			$this->form_validation->set_rules('dob','Date of Birth','xss_clean|trim|required');
			$this->form_validation->set_rules('value','Value','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('position','Position','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('salary','Salary','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('duration','Duration','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('p_club','Previous Club','xss_clean|trim');
			$this->form_validation->set_rules('jazz_name','Jazz Name','xss_clean|trim|required|');
			$this->form_validation->set_rules('jazz_no','Jazz Number','xss_clean|trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->player();
			}
			else{
				$m_session = $this->session->userdata('session_username');
			$club = $m_session['club_name'];
				$p_club=$this->input->post('p_club');

				    $dob = $this->input->post('dob');
				  //explode the date to get month, day and year
				  $dob = explode("-", $dob);
				  //get age from date or dob
				  $age = (date("md", date("U", mktime(0, 0, 0, $dob[0], $dob[1], $dob[2]))) > date("md")
				    ? ((date("Y") - $dob[0]) - 1)
				    : (date("Y") - $dob[0]));
				  // echo "Age is:" . $age;
				  if ($age<15) {
				  	$data['p_reg_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>sorry, the minimum age for a player is 15 years</strong></div><br><br><br><br>';
					$data['title'] = "Register Players";
			        $this->load->view('manager/m_heda',$data);
			        $this->load->view('manager/add_player',$data);
				  }
				 //  else if ($club==$p_club) {
				 //  	$data['p_reg_error'] = '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
     //                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
     //                              <strong>sorry, the the previous clu is similar to the current one</strong></div>';
					// $data['title'] = "Register Players";
			  //       $this->load->view('manager/m_heda',$data);
			  //       $this->load->view('manager/add_player',$data);
				 //  }
				  else{

				$result = $this->m_model->p_save();
				if ($result) {
					// $this->player();
					$data['p_reg_success'] = '<div class="alert alert-success alert-dismissible col-sm-10" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>player saved successfully '.anchor('players_prof',' click here ').' to view profile or '.anchor('p_reg',' here ') .' to add a new player'.
                                  '</strong></div><br><br><br><br>';
					$data['title'] = "Register Players";
			        $this->load->view('manager/m_heda',$data);
			        $this->load->view('manager/add_player',$data);
				}
				else{
					$data['p_reg_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>unable to register the player</strong></div><br><br><br><br>';
					$data['title'] = "Register Players";
			        $this->load->view('manager/m_heda',$data);
			        $this->load->view('manager/add_player',$data);
				}
			}
			}
			
	    }
	    public function players_prof()
	    {
	    	$data['title'] = "Player Profiles";
			        $this->load->view('manager/m_heda',$data);
			        $this->load->view('manager/p_disp',$data);
	    }
	    public function p_prof_edit($id)
	    {
	    	$data['result']=$this->m_model->p_prof_edit($id);
	    	$success=$data['result'];
	    	if ($data['result']) {
	    		$data['title'] = "Edit Player Profile";
			$this->load->view('manager/m_heda',$data);
			$this->load->view('manager/p_update',$data);
	    	}
	    	else{
	    		$this->session->set_flashdata('msg','<font color="red">an error occuured, try again later</font>');
	    		redirect('players_prof');
	    	}
	    	
	    }
	    public function p_prof_del($id)
	    {
	    	$result = $this->db->delete('player', array('id' => $id));
		if ($result) {
			$this->session->set_flashdata('msg','<font color="green">player successfully removed</font>');
		}
		else{
			$this->session->set_flashdata('msg','<font color="red">an error occuured, try again later</font>');
		}
		redirect('players_prof');
	    }
	    public function p_prof_update()
	    {
	    	$this->form_validation->set_rules('fname','First Name','xss_clean|trim|required');
			$this->form_validation->set_rules('lname','Last Name','xss_clean|trim|required');
			$this->form_validation->set_rules('phone','Phone Number','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('nationality','Nationality','xss_clean|trim|required|');
			$this->form_validation->set_rules('email','Email Address','xss_clean|trim|valid_email');
			$this->form_validation->set_rules('dob','Date of Birth','xss_clean|trim|required');
			$this->form_validation->set_rules('value','Value','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('position','Position','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('salary','Salary','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('duration','Duration','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('p_club','Previous Club','xss_clean|trim');
			$this->form_validation->set_rules('jazz_name','Jazz Name','xss_clean|trim|required|');
			$this->form_validation->set_rules('jazz_no','Jazz Number','xss_clean|trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['title'] = "Edit Player Profile";
			$this->load->view('manager/m_heda',$data);
			$this->load->view('manager/p_update',$data);
			}
			else{
	    	$player_id=$this->session->userdata('prof_sess');
	    	$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			$phone = $this->input->post('phone');
			$nationality = $this->input->post('nationality');
			$email = $this->input->post('email');
			$dob = $this->input->post('dob');
			$position = $this->input->post('position');
			$salary = $this->input->post('salary');
			$duration = $this->input->post('duration');
			$p_club = $this->input->post('p_club');
			$jazz_name = $this->input->post('jazz_name');
			$jazz_no = $this->input->post('jazz_no');
			$value = $this->input->post('value');

			$insert_data = array(
			'fname' => ucwords($fname),
			'lname' => ucwords($lname), 
			'position' => $position,
			'nationality' => ucwords($nationality),
			'dob'=>$dob,
			'previous_club' => ($p_club),
			'password' => md5('123456'),
			'jazz_name' => ucwords($jazz_name),
			'jazz_no' => $jazz_no,
			'phone' => $phone,
			'email' => $email,
			'value' => $value,
			'date' => date('Y-m-d H:i:s')
		);

		$insert_contract = array(
			'salary' => $salary,
			'contract_start' => date('Y-m-d H:i:s'),
			'duration' => $duration
			 );
		$this->db->where('player_id',$player_id);
		$result = $this->db->update('player',$insert_data);

		$this->db->where('player_id',$player_id);
		$result .= $this->db->update('contract',$insert_contract);
		if ($result) {
				$data['title'] = "Edit Player Profile";
				$data['p_reg_success'] = "information successfully updated " . anchor('players_prof','click here') . " to view";
			$this->load->view('manager/m_heda',$data);
			$this->load->view('manager/p_update',$data);
		}
		else{
			$data['p_reg_error'] = "information successfully updated " . anchor('players_prof','click here') . " to restart";
			$data['title'] = "Edit Player Profile";
			$this->load->view('manager/m_heda',$data);
			$this->load->view('manager/p_update',$data);
		}
	    }
	}
	     function p_trans_save(){
	    	$this->form_validation->set_rules('fname','First Name','xss_clean|trim|required');
			$this->form_validation->set_rules('lname','Last Name','xss_clean|trim|required');
			$this->form_validation->set_rules('phone','Phone Number','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('nationality','Nationality','xss_clean|trim|required|');
			$this->form_validation->set_rules('email','Email Address','xss_clean|trim|valid_email|');
			$this->form_validation->set_rules('dob','Date of Birth','xss_clean|trim|required');
			$this->form_validation->set_rules('value','Value','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('position','Position','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('salary','Salary','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('duration','Duration','xss_clean|trim|required|is_numeric');
			$this->form_validation->set_rules('p_club','Previous Club','xss_clean|trim');
			$this->form_validation->set_rules('jazz_name','Jazz Name','xss_clean|trim|required|');
			$this->form_validation->set_rules('jazz_no','Jazz Number','xss_clean|trim|required');
			// $this->form_validation->set_rules('password','Password','xss_clean|trim|required|min_length[6]');
			// $this->form_validation->set_rules('c_password','Confirm Password','xss_clean|trim|required|matches[password]');

			if ($this->form_validation->run() == FALSE) {
				$this->player();
			}
			else{
				

				$result = $this->m_model->p_trans_save();
				if ($result) {
					// $this->player();
					$data['p_reg_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>record saved successfully</strong></div><br><br><br><br>';
					$data['title'] = "Register Players";
			        $this->load->view('manager/m_heda',$data);
			        $this->load->view('manager/trans_player_reg',$data);
				}
				else{
					$data['p_reg_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>unable to register the player<br><br><br><br></strong></div>';
					$data['title'] = "Register Players";
			        $this->load->view('manager/m_heda',$data);
			        $this->load->view('manager/trans_player_reg',$data);
				}
			}
			
	    }
	    function p_edit()
	     {
	 
	      
	        $data['title'] = "Manage Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/player_edit', $data);
	    }
	 function tr_requests()
	     {
	        $data['title'] = "Manage Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/tr_requests', $data);
	    }
     public function p_get($id)
    {
    	$query = $this->db->get_where('player', array('id' => $id));
				foreach ($query->result() as $row) {
					$player = $row->player_id;
								
			}
				
				$query1 = $this->db->get_where('transfer', array('player_id' => $player));
				if ($query1->num_rows()>0) {
					$this->session->set_flashdata('error','<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>player already in the transfer list ' . anchor('tr_list','View list</strong></div><br><br><br><br>'));
						redirect('p_edit');
					}
			else{
    	
		$data['p_transfer'] = $this->m_model->p_get($player);	
		$result = $data['p_transfer'];
		if ($result) {
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/p_transfer', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/player_edit', $data);

			}	
		}
 		
		
    }
      public function p_buy_apply($id)
    {
    	$player_id="";
    	$admin = $this->session->userdata('session_username');
    	$club = $admin['club_name'];
    	$query1 = $this->db->get_where('transfer',array('id' =>$id));
    	foreach ($query1->result() as $row ) {
    		$player_id = $row->player_id;
    	}

    	
    	 $this->db->where('player_id', $player_id);
	     $this->db->where('dest_club', $club);
	    	 $query = $this->db->get('negotiation');
	    	 if ($query->num_rows()>0) {
	    	 	$this->session->set_flashdata('applied', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>vyour application was already received, ' . anchor('apps',' view your applications</strong></div><br><br><br><br>'));
	    	 	redirect('buy');
	    	 }
	    	 else{
    	
		$data['p_buy'] = $this->m_model->p_buy_apply($id);	
		$result = $data['p_buy'];
		if ($result) {
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/p_buy_apply', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/p_buy_apply', $data);

			}	
		}
 		
		
    }
    public function p_add_transfer()
    {
    	$result = $this->m_model->p_add_transfer();
    	if ($result) {
    		$data['p_reg_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Player successfully added to transfer list ' . anchor('p_edit','Add another</strong></div><br><br><br><br>');
    		// 'Player successfully added to transfer list <a href="p_edit">, Add another</a>';
    		$data['title'] = "Transfer Player";
		    $this->load->view('manager/m_heda', $data);
		    $this->load->view('manager/p_transfer', $data);
    	}
    	else{
    		$data['p_trans_error'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>player already in the transfer list ' . anchor('p_edit','Choose a different player</strong></div><br><br><br><br>');
    		$data['title'] = "Transfer Player";
		    $this->load->view('manager/m_heda', $data);
		    $this->load->view('manager/p_transfer', $data);
    	}
    }

	    function tr_list()
	     {
	 
	       
	        
	        $data['title'] = "Transfer Listed Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/transfer_list', $data);
	    }

          public function list_edit($id)
    {
    	
		$data['p_list_edit'] = $this->m_model->list_edit($id);	
		$result = $data['p_list_edit'];
		if ($result) {
				$data['title'] = "Transfer List";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/tr_list_edit', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer List";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/transfer_list', $data);

			}	
 		
		
    }
     public function list_edit_save()
    {
    	$result = $this->m_model->list_edit_save();
    	if ($result) {
    		$data['list_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>record successfully updated, ' . anchor('tr_list','update another</strong></div><br><br><br><br>');
    		 $data['title'] = "Buy Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/tr_list_edit', $data);
    	}
    	else{
    		$data['list_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured, '. anchor('tr_list','try again</strong></div><br><br><br><br>');
    		 $data['title'] = "Buy Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/tr_list_edit', $data);
    	}
    }
      public function list_remove($id)
	    {
	    	$this->db->select('*');
			$this->db->from('transfer');
			$this->db->where('id',$id);
			$query = $this->db->get();
			if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$player_id = $row->player_id;
				
			}
			$tables = array('transfer','negotiation','transfer_listed');
	    	$this->db->where('player_id',$player_id);
	    	$this->db->delete($tables );
	    	 // $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
       //                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       //                            <strong>player removed successfully</strong></div>');
	    	echo '<script> 
	    	alert("player removed successfully");
	    	window.location.href="'.base_url('tr_list').'";
	    	</script>';
	    	// redirect('tr_list');
		}
	    	
	    }
	    
	    function buy_player()
	     {        
	        $data['title'] = "Buy Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/buy_players', $data);
	    }
  
    function tr_nego_disp(){
	 
	        
        
	        $data['title'] = "Transfer Negotiations";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/transfer_negotiation', $data);
	    }
   
       function p_nego_disp()
	     {
	        
	        $data['title'] = "Player Negotiations";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/player_nego', $data);
	    }
            
  
    function tr_apps_disp()
	     {
	 
	        $data['title'] = "My Applications";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/tr_apps', $data);
	    }
  
    public function p_update_transfer()
    {
    	$admin = $this->session->userdata('session_username');
    	$club = $admin['club_name'];
    	$p_apply = $this->session->userdata('p_tran_apply_sess');
    	 $this->db->where('player_id', $p_apply['player_id']);
	     $this->db->where('dest_club', $club);
	    	 $query = $this->db->get('negotiation');
	    	 if ($query->num_rows()>0) {
	    	 	$data['buy_error'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>your application was already received, '. anchor('buy','restart application</strong></div><br><br><br><br>');
	    	 }
	    	 else{
    	$result = $this->m_model->p_update_transfer();
    	if ($result) {
    		$data['another'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>application successfully done, ' . anchor('buy','add another</strong></div><br><br><br><br>');
       	}
    	else{
    		$data['buy_error'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>your application was already received, '. anchor('buy','restart application</strong></div><br><br><br><br>');
    		 
    	}
    }
    	$data['title'] = "Buy Players";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/p_buy_apply', $data);
    }

	     

	     function renew(){
	    	$data['title'] = 'Renew Contract';
	    	$this->load->view('manager/m_heda', $data);
	    	$this->load->view('manager/renew_contract');
	    }

		function m_settings(){
	    	$data['title'] = 'System Settings';
	    	$this->load->view('manager/m_heda',$data);
	    	$this->load->view('manager/m_settings');
	    }
	    public function nego_ok($id)
	    {
	    	$query = $this->db->get_where('negotiation',array('id' => $id ));
	    	foreach ($query->result() as $row) {
				$reply = $row->b_reply;
				$dest_club = $row->dest_club;
			}
			if ($reply == 'REJECTED') {
				 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $dest_club . ', click reject to terminate</strong></div><br><br><br><br>');
			}
			else{

	    	$update_data = array(
	    		's_reply' => 'OK'
	    		);
	    	$this->db->update('negotiation',$update_data);
	    	 $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer accepted successfully</strong></div><br><br><br><br>');
	    	}
	    	redirect('nego');
	    }
	    	     public function nego_edit($id)
    {

    	$query = $this->db->get_where('negotiation',array('id' => $id ));
	    	foreach ($query->result() as $row) {
				$reply = $row->b_reply;
				$dest_club = $row->dest_club;
			}
			if ($reply == 'REJECTED') {
				 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $dest_club .', click reject to terminate</strong></div><br><br><br><br>');
				 redirect('nego');
			}
			else{
		$data['p_nego'] = $this->m_model->nego_edit($id);	
		$result = $data['p_nego'];
		if ($result) {
				$data['title'] = "Transfer Negotiations";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/nego_edit', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/transfer_negotiation', $data);

			}
			}	
 		
		
    }
	    public function nego_cancel($id)
	    {
	  //   	$query = $this->db->get_where('negotiation',array('id' => $id ));
	  //   	foreach ($query->result() as $row) {
			// 	$reply = $row->b_reply;
			// 	$dest_club = $row->dest_club;
			// }
			// if ($reply == 'REJECTED') {
			// 	 $this->session->set_flashdata('error', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
   //                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   //                                <strong>Transfer was terminated by ' . $dest_club . ', click reject to terminate</strong></div><br><br><br><br>');
			// 	 redirect('nego');
			// }
			// else{
	    	$this->db->where('id',$id);
	    	$update_data = array(
	    		's_reply' => 'REJECTED'
	    		);
	    	$this->db->update('negotiation',$update_data);
	    	 // $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
       //                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       //                            <strong>Transfer terminated successfully</strong></div>');
	    	echo '<script> 
	    	alert("transfer terminated successfully");
	    	window.location.href="'.base_url('nego').'";
	    	</script>';
	    	// }
	    	
	    }


     public function nego_edit_save()
    {
    	$result = $this->m_model->nego_edit_save();
    	if ($result) {
    		$data['nego_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>information successfully saved, ' . anchor('nego','return to negotiations') .'</strong></div><br><br><br><br>';
    	  	}
    	else{
    		$data['nego_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured, '. anchor('nego','try again') . '</strong></div><br><br><br><br>';
    		
    	}
    	 $data['title'] = "Transfer Negotiations";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/nego_edit', $data);
    }
	    public function buy_ok($id)
	    {
	    	$query = $this->db->get_where('negotiation',array('id' => $id ));
	    	foreach ($query->result() as $row) {
				$reply = $row->s_reply;
				$source_club = $row->source_club;
			}
			if (empty($reply)) {
				$this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>No reply from ' . $source_club . ', wait for the response</strong></div><br><br><br><br>');
				redirect('apps');
			}
			else if ($reply == 'REJECTED') {
				 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $source_club . ', click reject to terminate</strong></div><br><br><br><br>');
				 redirect('apps');
			}
	    		else{
	    			$this->db->where('id',$id);
	    	$update_data = array(
	    		'b_reply' => 'OK'
	    		);
	    	$this->db->update('negotiation',$update_data);

		$data['b_ok'] = $this->m_model->buy_ok($id);	
		$result = $data['b_ok'];
		echo "<script>alert('transfer accepted successfully,set the player salary');</script>";
		if ($result) {
				$data['title'] = "Set Salary";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/buy_ok', $data);
		       
			}
			else{
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/buy_ok', $data);

			}
			}
	    	
	    }
	     public function buy_ok_save()
    {
    	$nego_sess = $this->session->userdata('p_level_sess');
    	$id=$nego_sess['id'];
    	$query= $this->db->get_where('negotiation',array('id' =>$id, 'level' => 'PLAYER'));
    	if ($query->num_rows()>0) {
    		$data['buy_error'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>your application was already received, '. anchor('p_nego','return to negotiations</strong></div><br><br><br><br>');
    	}
    	else{

    	$result = $this->m_model->buy_ok_save();
    	if ($result) {
    		$data['buy_success'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>salary successfully offered, ' . anchor('p_nego','return to negotiations</strong></div><br><br><br><br>');
    		
    	}
    	else{
    		$data['buy_error'] = '<div class="alert alert-danger alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured,check your network connection '. anchor('p_nego','try again</strong></div><br><br><br><br>');
    		 
    	}
    }
    	$data['title'] = "Transfer Negotiations";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/buy_ok', $data);
    }
	    	       public function buy_edit($id)
    {

    	$query = $this->db->get_where('negotiation',array('id' => $id ));
	    	foreach ($query->result() as $row) {
				$reply = $row->s_reply;
				$source_club = $row->source_club;
			}
			if ($reply == 'REJECTED') {
				 $this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $source_club .', click reject to terminate</strong></div><br><br><br><br>');
				 redirect('apps');
			}
			else{
		$data['b_nego'] = $this->m_model->buy_edit($id);	
		$result = $data['b_nego'];
		if ($result) {
				$data['title'] = "Transfer Negotiations";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/buy_edit', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/tr_apps', $data);

			}
			}	
 		
		
    }
	    public function buy_cancel($id)
	    {
	  //   	$query = $this->db->get_where('negotiation',array('id' => $id ));
	  //   	foreach ($query->result() as $row) {
			// 	$reply = $row->s_reply;
			// 	$source_club = $row->source_club;
			// }
			// if ($reply == 'REJECTED') {
			// 	 $this->session->set_flashdata('error', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
   //                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   //                                <strong>Transfer was terminated by ' . $source_club .', click reject to terminate</strong></div><br><br><br><br>');
			// 	 redirect('apps');
			// }
			// else{
	    	$this->db->where('id',$id);
	    	$update_data = array(
	    		'b_reply' => 'REJECTED'
	    		);
	    	$this->db->update('negotiation',$update_data);
	    	 // $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
       //                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       //                            <strong>Transfer terminated successfully</strong></div>');
	    	echo '<script> 
	    	alert("Transfer terminated successfully");
	    	window.location.href="'.base_url('apps').'";
	    	</script>';
	    	// }
	    	
	    }
	      public function buy_edit_save()
    {
    	$result = $this->m_model->buy_edit_save();
    	if ($result) {
    		$data['buy_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>information successfully saved, ' . anchor('apps','return to negotiations</strong></div><br><br><br><br>');
    		 $data['title'] = "Transfer Applications";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/buy_edit', $data);
    	}
    	else{
    		$data['buy_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured, '. anchor('apps','try again</strong></div><br><br><br><br>');
    		 $data['title'] = "Transfer Applications";
	        $this->load->view('manager/m_heda', $data);
	        $this->load->view('manager/buy_edit', $data);
    	}
    }
    public function m_nego_ok($id)
        {
        	$this->db->select('*');
        	$this->db->from('negotiation');
        	$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
			$p_transfered="";
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $reply = $row->p_reply;
                $name = $row->jazz_name;
             
            }
            
            if ($reply == 'REJECTED') {
                 $this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $name .', click reject to terminate</strong></div><br><br><br><br>');
            }
            else if (empty($reply)) {
                 $this->session->set_flashdata('error', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>please wait for a reply from ' . $name.'</strong></div><br><br><br><br>');
            }
            else{

            $update_data = array(
                'm_reply' => 'OK'
                );

            $sess = array('nego_id' =>$id);
            $this->session->set_userdata('nego_reg',$sess);
             $this->db->where('id',$id);
            $this->db->update('negotiation',$update_data);

            $q=$this->db->get_where('negotiation',array('id' => $id));
            foreach ($query->result() as $row) {
            	$player_id=$row->player_id;
            	
            }
            $this->session->set_userdata('p_update_sess',$player_id);

             $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer accepted successfully. ' . anchor('p_trans_reg','Register the Player</strong></div><br><br><br><br>'));
            }
            redirect('p_nego');
        }
             public function m_nego_cancel($id)
        {
            $this->db->select('*');
        	$this->db->from('negotiation');
        	$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $reply = $row->p_reply;
                $name = $row->jazz_name;
            }
            // if ($reply == 'REJECTED') {
            //      $this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
            //                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            //                       <strong>Transfer was terminated by ' . $name . ', click reject to terminate</strong></div>');
            //      redirect('p_nego');
            // }
            if (empty($reply)) {
                 $this->session->set_flashdata('error', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>please wait for a reply from ' . $name .'</strong></div><br><br><br><br>');
            redirect('p_nego');
            }
            else{

            $update_data = array(
                'm_reply' => 'REJECTED'
                
                );
            $this->db->where('id',$id);
            $this->db->update('negotiation',$update_data);
             // $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
             //                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             //                      <strong>Transfer terminated successfully</strong></div>');
            echo '<script> 
	    	alert("Transfer terminated successfully");
	    	window.location.href="'.base_url('p_nego').'";
	    	</script>';
            }
            
        }
         public function m_nego_edit($id)
        {
           $this->db->select('*');
        	$this->db->from('negotiation');
        	$this->db->join('player','player.player_id=negotiation.player_id');
			$this->db->where('negotiation.id',$id);
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $reply = $row->p_reply;
                $name = $row->jazz_name;
            }
            if ($reply == 'REJECTED') {
                 $this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $name . ', click reject to terminate </strong></div><br><br><br><br>');
                 redirect('p_nego');
            }
            else if (empty($reply)) {
                 $this->session->set_flashdata('error', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>please wait for a reply from ' . $name .'</strong></div><br><br><br><br>');
                 redirect('p_nego');
            }
                else{
        $data['p_nego'] = $this->m_model->salary_edit($id);   
        $result = $data['p_nego'];
        if ($result) {
                $data['title'] = "Salary Negotiations";
                $this->load->view('manager/m_heda', $data);
                $this->load->view('manager/salary_nego', $data);
               
            }
            else{
                
                 $this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>unable to compete request </strong></div><br><br><br><br>');
                 redirect('p_nego');

            }
            }
           
        }
         public function salary_nego_save()
    {
        $result = $this->m_model->salary_nego_save();
        if ($result) {
            $data['nego_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>information successfully saved, ' . anchor('p_nego','Return to negotiations</strong></div><br><br><br><br>');
             $data['title'] = "Transfer Negotiations";
            $this->load->view('manager/m_heda', $data);
            $this->load->view('manager/salary_nego', $data);
        }
        else{
            $data['nego_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured, '. anchor('p_request','try again</strong></div><br><br><br><br>');
             $data['title'] = "Transfer Negotiations";
            $this->load->view('manager/m_heda', $data);
            $this->load->view('manager/salary_nego', $data);
        }
    }
    public function m_req_ok($id)
    {
    	$req_update = array('reply' => 'ACCEPTED');
    	$this->db->where('id',$id);
    	$this->db->update('transfer_request',$req_update);
    	$query = $this->db->get_where('transfer_request', array('id' => $id));
				foreach ($query->result() as $row) {
					$player = $row->player_id;
								
			}
				
				$query1 = $this->db->get_where('transfer', array('player_id' => $player));
				if ($query1->num_rows()>0) {
					$this->session->set_flashdata('error','<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>player already in the transfer list ' . anchor('tr_list','View list</strong></div><br><br><br><br>'));
						redirect('tr_requests');
					}
			else{
    	
		$data['p_transfer'] = $this->m_model->p_get($player);	
		$result = $data['p_transfer'];
		if ($result) {
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/p_transfer', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Transfer Player";
		        $this->load->view('manager/m_heda', $data);
		        $this->load->view('manager/player_edit', $data);

			}	
		}
    }
      public function m_req_reject($id)
    {
    	$req_update = array('reply' => 'REJECTED');
    	$this->db->where('id',$id);
    	$result = $this->db->update('transfer_request',$req_update);
    	if ($result) {

    		$this->session->set_flashdata('ok','<div class="alert alert-success alert-dismissible col-sm-4" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>request successfully rejected</strong></div><br><br><br><br>');
    	}
    	else{
    		$this->session->set_flashdata('error','div class="alert alert-danger alert-dismissible col-sm-4" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>unable to complete request, try again later</strong></div><br><br><br><br>');
    	}
    	
    	redirect('tr_requests');
    }
    public function manager_p_report()
    {
    	$data['title'] = "Player Reports";
    	$this->load->view('manager/m_heda', $data);
		$this->load->view('manager/m_player_report', $data);
    }
    public function players_sold()
    {
    	$data['title'] = "Players Sold";
    	$this->load->view('manager/m_heda', $data);
		$this->load->view('manager/players_sold', $data);
    }
    public function players_bought()
    {
    	$data['title'] = "Players Bought";
    	$this->load->view('manager/m_heda', $data);
		$this->load->view('manager/players_bought', $data);
    }
    public function pass_change()
    {
      $this->form_validation->set_rules('pass','Password','xss_clean|trim|required|min_length[6]');
        $this->form_validation->set_rules('con_pass','Confirm Password','xss_clean|trim|required|matches[pass]|min_length[6]');
    
    $this->form_validation->set_rules('c_pass','Password','xss_clean|trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->load->view('manager/m_heda');
      $this->load->view('manager/m_settings');
    }
    else{
    	$cur_pass=$this->input->post('c_pass');
    	$password="";
      $manager = $this->session->userdata('session_username');
 	$username = $manager['uname'];

        $this->db->where('uname',$username);
        $query=$this->db->get('manager');
        foreach ($query->result() as $row ) {
          $password=$row->password;
        }
        
        if ($password != md5($cur_pass)) {
          $data['error'] = 'sorry the current password is incorrect';
        }
        else{
          $new_pass=$this->input->post('pass');

        $update_data = array('password' => md5($new_pass));

        $this->db->where('uname',$username);
        $result=$this->db->update('manager',$update_data);
        if ($result) {
          $data['success'] = 'password changed successfully';
           
        }
        else{
          $data['error'] = 'sorry an error occurred, try again later';
        }

        }
        
        $this->load->view('manager/m_heda',$data);
            $this->load->view('manager/m_settings',$data);


        
    }
  }
    

	      
   
	  
	}
?>