<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class C_ctlr extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	function index(){
		$this->load->view('home');
	}
	function logout(){
		$this->session->sess_destroy();
		// $this->session->set_flashdata('error','<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
  //                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  //                                 <strong>please login to continue</strong>');
		$this->index();
	}
	public function contact()
	{
		$this->load->view('contact');
	}
	function send_email(){
		$this->load->library('email');

		$email =$this->input->post('email');
		$name =$this->input->post('name');
		$phone =$this->input->post('phone');
		$subject =$this->input->post('subject');
		$message =$this->input->post('message');
		$this->email->from($email, $name);
		$this->email->to('mangsmato@gmail.com'); 
		// $this->email->cc('another@another-example.com'); 
		// $this->email->bcc('them@their-example.com'); 

		$this->email->subject($subject);
		$this->email->message('Phone: ' . $phone . "\n" . $message);	


		$result = $this->email->send();
		if ($result) {
			$data['success'] = 'email sent successfully';
			$this->load->view('contact',$data);
		}
		else{
			$data['error'] = 'an error encountered';
			$this->load->view('contact',$data);
		}
	



	}
}
?>