<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class A_ctlr extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	function index(){
		$this->load->view('admin/admin');
	}
	
	public function a_login(){
		$this->form_validation->set_rules('username','Username','xss_clean|trim|required');
		$this->form_validation->set_rules('password','Password','xss_clean|trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin/admin');
		}
		else{
			$username=$this->input->post('username');
			$password=md5($this->input->post('password'));
			$result=$this->a_model->a_login($username,$password);
			if ($result) {

				// redirect('m_reg');
				$sess_data = array('username' =>$username);
				$this->session->set_userdata('admin_sess', $sess_data);
				$this->club();
			}
			else{
						
				$data["error"]='<div class="alert alert-danger alert-dismissible" role="alert" >
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <strong>incorrect username or password</strong></div>';
				$this->load->view('admin/admin',$data);
				
			}
		}
		
	}
	function club(){
		$data['title'] = "Add New Club";
		$this->load->view('admin/heda',$data);
		$this->load->view('admin/add_club',$data);
		
	}
	public function c_reg(){
		$this->form_validation->set_rules('club_name','Club Name','trim|xss_clean|required|is_unique[club.name]');
  		$this->form_validation->set_rules('stadium','Stadium','trim|xss_clean|required|');
  		$this->form_validation->set_rules('form_yr','Formation  Year','trim|xss_clean|required|');
  		$this->form_validation->set_rules('address','Address','trim|xss_clean|required|');
  		$this->form_validation->set_rules('city','City','trim|xss_clean|required|');
  		$this->form_validation->set_rules('email','Club Email','trim|xss_clean|required|is_unique[club.club_email]|valid_email');
  		$this->form_validation->set_rules('phone','Telephone','trim|xss_clean|required|is_numeric|is_unique[club.club_phone]|min_length[10]|max_length[15]');
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "club register";
				$data['tab'] = 'club_info';
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
		}
		else{	
			$club_name = $this->input->post('club_name');		
			$result = $this->a_model->c_reg();
			if ($result) {
				// $data['success'] = ucwords($club_name) . " successfully saved";
				echo "<script>alert('information saved successfully');</script>";
				$data['title'] = "club register";
				$data['tab'] = 'manager_info';
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
			else{
				echo "<script>alert('unable to compete request');</script>";
				$data['tab'] = 'club_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
		}
	}
   
    public function c_get($id)
    {
    	
		$data['edit_club'] = $this->a_model->c_get($id);	
		$result = $data['edit_club'];
		if ($result) {
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/add_club', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/add_club', $data);

			}	
 		
		
    }
     public function m_update(){
     	$this->form_validation->set_rules('fname','First Name','xss_clean|trim|required');
		$this->form_validation->set_rules('lname','Last Name','xss_clean|trim|required');
		// $this->form_validation->set_rules('uname','Username','xss_clean|trim|required|is_unique[manager.uname]');
		$this->form_validation->set_rules('m_phone','Phone Number','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
		// $this->form_validation->set_rules('club','Club Name','xss_clean|trim|required|');
		$this->form_validation->set_rules('m_email','Email Address','xss_clean|trim|valid_email|');
		$this->form_validation->set_rules('password','Password','xss_clean|trim|required|min_length[6]');
		$this->form_validation->set_rules('c_password','Confirm Password','xss_clean|trim|required|matches[password]');

		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_update', $data);
		        
		}
		else{
			
			$result = $this->a_model->m_update();
			if ($result) {
				$data['c_success'] = "information updated successfully " .anchor('m_manage_disp','click here') . " to update more";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_update', $data);

			}
			else{
				$data['c_error'] = "an account already exists, check you have unique email  or phone";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_update', $data);
		        
			}
		}
     }
	function c_edit()
	 {
 
  		$data['title'] = "Manage Clubs";
        $this->load->view('admin/heda', $data);
        $this->load->view('admin/club_edit', $data);
    }
    function m_reg(){
		
		$this->form_validation->set_rules('fname','First Name','xss_clean|trim|required');
		$this->form_validation->set_rules('lname','Last Name','xss_clean|trim|required');
		// $this->form_validation->set_rules('uname','Username','xss_clean|trim|required|is_unique[manager.uname]');
		$this->form_validation->set_rules('m_phone','Phone Number','xss_clean|trim|required|is_numeric|is_unique[manager.phone]|min_length[10]|max_length[15]');
		// $this->form_validation->set_rules('club','Club Name','xss_clean|trim|required|is_unique[manager.club]');
		$this->form_validation->set_rules('m_email','Email Address','xss_clean|trim|valid_email|is_unique[manager.email]');
		// $this->form_validation->set_rules('password','Password','xss_clean|trim|required|min_length[6]');
		// $this->form_validation->set_rules('c_password','Confirm Password','xss_clean|trim|required|matches[password]');

			$data['clubs'] = $this->a_model->get_clubs();
		if ($this->form_validation->run() == FALSE) {
				$data['tab'] = 'manager_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
		}
		else{

			$club_name = $this->session->userdata('club_reg_sess');
			$club = $club_name['name'];
			if (empty($club_name)) {
				$data['m_error'] = 'error encountered, please complete the previous step successfully';
				$data['tab'] = 'manager_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
			else{
     		
			$result = $this->a_model->m_reg($club);
			if ($result) {
				echo "<script>alert('information saved successfully');</script>";
				$data['tab'] = 'stake_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
			else{
				// echo "<script>alert('unable to compete request');</script>";
				$error = $this->db->_error_message();
				$data['m_error'] = 'unable to compete request, try again later' . $error;
				$data['tab'] = 'manager_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
		}
		}
		
	}
	function stake_reg(){
		
		$this->form_validation->set_rules('own_name','Owner Name','xss_clean|trim|required');
		$this->form_validation->set_rules('own_phone','Owner Contact','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
		$this->form_validation->set_rules('own_shares','Owner Shares','xss_clean|trim|required');

		$this->form_validation->set_rules('part_name','Partner Name','xss_clean|trim|required');
		$this->form_validation->set_rules('part_phone','Partner Contact','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
		$this->form_validation->set_rules('part_shares','Partner Shares','xss_clean|trim|required');
		
		// $data['clubs'] = $this->a_model->get_clubs();
		if ($this->form_validation->run() == FALSE) {
				$data['tab'] = 'stake_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
		}
		else{
			$club_name = $this->session->userdata('club_reg_sess');
			if (empty($club_name)) {
				$data['stake_error'] = 'error encountered, please complete the previous steps successfully';
				$data['tab'] = 'stake_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
			else{
     		$club = $club_name['name'];
			$result = $this->a_model->stake_reg($club);
			if ($result) {
				echo "<script>alert('information saved successfully');</script>";
				
				$this->c_edit();
			}
			else{
				// echo "<script>alert('unable to compete request');</script>";
				$data['stake_error'] = 'unable to compete request, try again later';
				$data['tab'] = 'stake_info';
				$data['title'] = "club register";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/add_club',$data);
			}
		}
		}
		
	}
	 public function m_get($id)
    {
    	
		$data['edit_club'] = $this->a_model->m_get($id);	
		$result = $data['edit_club'];
		if ($result) {
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_update', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_update', $data);
			}	
 		
		
    }
    public function c_delete($id)
    {
    	$result = $this->a_model->c_delete($id);
    	if ($result) {
    		echo "<script>alert('club deleted successfully');</script>";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/club_edit');
    	}
    	else{
    		echo "<script>alert('unable to complete request');</script>";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/club_edit', $data);
    	}
    }
    public function c_id_update($id)
    {
    	$data['title'] = "Update Clubs";
    	$data['result'] = $this->a_model->c_id_update($id);
    	$this->load->view('admin/heda', $data);
		  $this->load->view('admin/c_update',$data);
    }
    public function c_update()
    {
    	
		$this->form_validation->set_rules('own_name','Owner Name','xss_clean|trim|required');
		$this->form_validation->set_rules('own_phone','Owner Contact','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
		$this->form_validation->set_rules('own_shares','Owner Shares','xss_clean|trim|required');

		$this->form_validation->set_rules('part_name','Partner Name','xss_clean|trim|required');
		$this->form_validation->set_rules('part_phone','Partner Contact','xss_clean|trim|required|is_numeric|min_length[10]|max_length[15]');
		$this->form_validation->set_rules('part_shares','Partner Shares','xss_clean|trim|required');

		$this->form_validation->set_rules('club_name','Club Name','trim|xss_clean|required');
  		$this->form_validation->set_rules('stadium','Stadium','trim|xss_clean|required|');
  		$this->form_validation->set_rules('form_yr','Formation  Year','trim|xss_clean|required|');
  		$this->form_validation->set_rules('address','Address','trim|xss_clean|required|');
  		$this->form_validation->set_rules('city','City','trim|xss_clean|required|');
  		$this->form_validation->set_rules('email','Club Email','trim|xss_clean|required|valid_email');
  		$this->form_validation->set_rules('phone','Telephone','trim|xss_clean|required|is_numeric|min_length[10]|max_length[15]');
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "Update Clubs";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/c_update',$data);
		}
		else{	
			$club=$this->session->userdata('club_update_sess');
			$insert_data = array(
			'name' => ucwords($this->input->post('club_name')),
			'stadium' =>  ucwords($this->input->post('stadium')),
			'reg_date' => date('Y-m-d H:i:s'),
			'form_yr' => $this->input->post('form_yr'),
			'address' =>  ucwords($this->input->post('address')),
			'city' =>  ucwords($this->input->post('city')),
			'club_phone' =>  $this->input->post('phone'),
			'club_email' =>  $this->input->post('email'),

			);

			$own_array = array(
			'club' =>$club , 
			'own_name' =>ucwords($this->input->post('own_name')) ,
			'own_shares' =>$this->input->post('own_shares') ,
			'own_phone' =>$this->input->post('own_phone') ,
			);

		$part_array = array(
			'club' =>$club , 
			'part_name' =>ucwords($this->input->post('part_name')) ,
			'part_shares' =>$this->input->post('part_shares') ,
			'part_phone' =>$this->input->post('part_phone') ,
			);
		$this->db->where('name',$club);
		$result = $this->db->update('club',$insert_data);

		$this->db->where('club',$club);
		$result = $this->db->update('owner',$own_array);

		$this->db->where('club',$club);
		$result .= $this->db->update('partners',$part_array);
		if ($result) {
			$data['success'] = "information successfully updated " . anchor('c_edit', " view details");
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/c_update',$data);
		}
		else{
			$data['error'] = "sorry an error occurred, try again later";
				$this->load->view('admin/heda',$data);
				$this->load->view('admin/c_update',$data);
		}
		
			
		}
    }
      public function m_delete($id)
    {
    	$result = $this->a_model->m_delete($id);
    	if ($result) {
    		echo "<script>alert('manager deleted successfully');</script>";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_manage', $data);
    	}
    	else{
    		echo "<script>alert('unable to complete request');</script>";
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_manage', $data);
    	}
    }
    public function m_assign($id)
    {
    	
		$data['edit_club'] = $this->a_model->m_assign($id);	
		$result = $data['edit_club'];
		if ($result) {
				$data['title'] = "Update Clubs";
		        $this->load->view('admin/heda', $data);
		        $this->load->view('admin/m_update', $data);
		       
			}
			else{
				
				
				echo "<script>alert('unable to compete request');</script>";

			}	
 		
		
    }
    
	function m_manage()
    {
        $this->datatables->select('id,fname,lname,uname,phone,email,add_date,club')
        				 ->unset_column('id')
         				 ->from('manager');
        $this->datatables->add_column('action', m_helper('$1'), 'id');
 
        echo $this->datatables->generate();
        
    }
     function m_manage_disp()
     {
 
             
        $data['title'] = "Manage Managers";
        $this->load->view('admin/heda', $data);
        $this->load->view('admin/m_manage', $data);
    }
    function a_settings(){
    	$data['title'] = 'System Settings';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/a_settings');
    }
    public function c_report()
    {

    	$data['title'] = 'Admin Club Reports';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/c_report');

    }
    public function m_report()
    {
    	$data['title'] = 'Admin Manager Reports';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/m_report');
    }
    public function admin_p_report()
    {
    	$data['title'] = 'Player Reports';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/player_report');
    }
    public function admin_tr_reports()
    {
    	$data['title'] = 'Admin Transfer Reports';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/tr_report');
    }
    public function report_clubs()
    {

    	$data['clubs'] = $this->a_model->report_clubs();
		if ($data['clubs'] ) {
			$radio = $this->input->post('search');
		$start = $this->input->post('start');
    	$end = $this->input->post('end');
    	if ($radio=='all') {
    		$data['heading'] = "<strong>KFF: All Registered Clubs</strong>";
    	}
    	else{
    		$data['heading'] = "<strong> KFF: Clubs Registered Between " . $start ." AND " . $end . "</strong>";
    	}
			$data['title'] = 'Admin Club Reports';
	    	$this->load->view('admin/heda',$data);
	    	$this->load->view('admin/c_report');
		}
			
		
		else{
			$data['error'] = 'unable to complete request, please an appropriate range of dates';
			$data['title'] = 'Admin Club Reports';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/c_report');
		}
    }
        public function report_managers(){

    	$data['managers'] = $this->a_model->report_managers();
		if ($data['managers'] ) {
			$radio = $this->input->post('search');
		$start = $this->input->post('start');
    	$end = $this->input->post('end');
    	if ($radio=='all') {
    		$data['heading'] = "<strong>All Registered Managers</strong>";
    	}
    	else{
    		$data['heading'] = "<strong> Managers Registered Between " . $start ." AND " . $end . "</strong>";
    	}
			$data['title'] = 'Admin Manager Reports';
	    	$this->load->view('admin/heda',$data);
	    	$this->load->view('admin/m_report');
		}
			
		
		else{
			$data['error'] = 'unable to complete request, please an appropriate range of dates';
			$data['title'] = 'Admin Club Reports';
	    	$this->load->view('admin/heda',$data);
	    	$this->load->view('admin/m_report');
		}
    }
     public function report_players()
    {

    	$data['players'] = $this->a_model->report_players();
		if ($data['players'] ) {
			$radio = $this->input->post('search');
		$club = $this->input->post('club');
    	if ($radio=='all') {
    		$data['heading'] = "<strong>All Registered players</strong>";
    	}
    	else{
    		$data['heading'] = "<strong>  Players for " . $club . "</strong>";
    	}
			
		}		
		else{
			$data['error'] = 'the club has not registered any player yet';
		}
			$data['title'] = 'Player Reports';
    	$this->load->view('admin/heda',$data);
    	$this->load->view('admin/player_report',$data);
		
    }
      public function report_transfers(){
      	$radio = $this->input->post('search');
			$transfer_type = $this->input->post('transfer_type');
	    	$level = $this->input->post('level');
	    	$status = $this->input->post('status');

    	$data['transfers'] = $this->a_model->report_transfers($radio,$transfer_type,$level,$status);
    	$transfer = $data['transfers'];
		if ($transfer ) {
			
    	if ($radio=='all') {
    		$data['heading'] = "<strong>All Transfers this season</strong>";
    	}
    	else if($radio=='t_type'){
    		$data['heading'] = "<strong>" . $transfer_type . " Transfers. </strong>" ;
    	}
    	else if($radio=='t_level'){
    		$data['heading'] = "<strong>Transfers at " . $level ." Level </strong>" ;
    	}
    	else if($radio=='t_status'){
    		$data['heading'] = "<strong>" . $status . " Transfers. </strong>" ;
    	}
			$data['title'] = 'Admin Transfer Reports';
	    	$this->load->view('admin/heda',$data);
	    	$this->load->view('admin/tr_report');
		}
			
		
		else{
			$data['error'] = 'no transfer has occurred within that category';
			$data['title'] = 'Admin Transfer Reports';
	    	$this->load->view('admin/heda',$data);
	    	$this->load->view('admin/tr_report');
		}
    }
  
    public function c_pdf()
    {
    	// $radio = $this->input->post('search');
    	// if ($radio=='all') {
    		$this->db->select("*");
		$this->db->from('club');
		$this->db->join('manager','manager.club=club.name','left');
		
    	// }
    // 	else if($radio=='date'){
    // 		$start = $this->input->post('start');
    // 		$end = $this->input->post('end');
    // 		if (empty($start)||empty($end)) {
    // 			return false;
    // 		}
    // 		else{
    // 			$this->db->select("*");
				// $this->db->from('manager');
				// $this->db->join('club','manager.club=club.name');
				// $this->db->where("manager.add_date BETWEEN '$start' AND '$end'");
				
    // 		}
    // 	}
		$this->cezpdf->ezText('THE FOOTBALL KENYA FEDERATION', 16, array('justification' => 'center'));//text,font size, config options
      $this->cezpdf->ezText("your trusted football association", 8, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-20);//white space
    	$query = $this->db->get();
		if ($query->num_rows()>0) {
			$no=0;
			foreach ($query->result() as $row) {
				$no++;
				$pdf_data[] = array(
					'no'=> $no,
					'club' => $row->name , 
					'stadium' => $row->stadium,
					'manager' => $row->fname . " " . $row->lname,
					'reg_date' =>  $row->reg_date
					);
			}
			$col_names = array(
				'no' => 'No.',
		        'club' => 'Club Name',
				'stadium' =>'Stadium',
				'manager' => 'Manager',
				'reg_date'  => 'Registration Date'
		    );
		      $this->cezpdf->ezTable($pdf_data, $col_names, 'Registered Clubs', array('width'=>550));
     		 $this->cezpdf->ezStream();
		}
    }
      public function m_pdf()
    {
    	// $radio = $this->input->post('search');
    	// if ($radio=='all') {
    		$this->db->select("*");
		$this->db->from('manager');
		$this->db->join('club','manager.club=club.name');
		
    	// }
    // 	else if($radio=='date'){
    // 		$start = $this->input->post('start');
    // 		$end = $this->input->post('end');
    // 		if (empty($start)||empty($end)) {
    // 			return false;
    // 		}
    // 		else{
    // 			$this->db->select("*");
				// $this->db->from('manager');
				// $this->db->join('club','manager.club=club.name');
				// $this->db->where("manager.add_date BETWEEN '$start' AND '$end'");
				
    // 		}
    // 	}
		prep_pdf(); 
		$this->cezpdf->ezText('THE FOOTBALL KENYA FEDERATION', 16, array('justification' => 'center'));
      $this->cezpdf->ezText("your trusted football association", 8, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-20);//white space
    	$query = $this->db->get();
		if ($query->num_rows()>0) {
			$no=0;
			foreach ($query->result() as $row) {
				$no++;
				$pdf_data[] = array(
					'no' => $no,
					'name' => $row->fname . " " . $row->lname,
					'username' => $row->uname,
					'phone'	=> $row->phone,
					'email' =>	$row->email,
					'club' => $row->name , 
					'reg_date' =>  $row->add_date 
				);
			}
			$col_names = array(
		        'no' => 'No.',
				'name' => 'Name',
				'username' => 'Username',
				'phone' => 'Phone',
				'email' => 'Email',
				'club' => 'Club',
				'reg_date' => 'Registration Date'
		    );
		      $this->cezpdf->ezTable($pdf_data, $col_names, 'Registered Managers', array('width'=>550));
     		 $this->cezpdf->ezStream();
		}
    }
    public function tr_pdf()
    {
    	// $radio = $this->input->post('search');
    	// if ($radio=='all') {
    		$this->db->select("*");
			$this->db->from('transfer');
			$this->db->join('player','transfer.player_id=player.player_id');
		
    	// }
    // 	else if($radio=='date'){
    // 		$start = $this->input->post('start');
    // 		$end = $this->input->post('end');
    // 		if (empty($start)||empty($end)) {
    // 			return false;
    // 		}
    // 		else{
    // 			$this->db->select("*");
				// $this->db->from('manager');
				// $this->db->join('club','manager.club=club.name');
				// $this->db->where("manager.add_date BETWEEN '$start' AND '$end'");
				
    // 		}
    // 	}
		prep_pdf(); 
		$this->cezpdf->ezText('THE FOOTBALL KENYA FEDERATION', 16, array('justification' => 'center'));
      $this->cezpdf->ezText("your trusted football association", 8, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-20);//white space
    	$query = $this->db->get();
		if ($query->num_rows()>0) {
			$no=0;
			foreach ($query->result() as $row) {
				$no++;
				$pdf_data[] = array(
					'no' => $no,
					'id' => $row->player_id,
					'name' => $row->fname . " " . $row->lname,
					'from' => $row->source_club, 
					'to' => $row->dest_club, 
					'price' => $row->transfer_price,
					'salary' => $row->transfer_salary,
					'start_date' => $row->transfer_date, 
					'type' => $row->transfer_type,
					'level' => $row->level, 
					'status' => $row->status, 
					'end_date' => $row->date_complete
				);
			}
			$col_names = array(
		        'no' => 'No',
				'id' => 'Player Id',
				'name' => 'Name',
				'from' => 'From',
				'to' => 'To',
				'price' => 'Price',
				'salary' => 'Salary',
				'start_date' => 'Start Date',
				'type' => 'Type',
				'level' => 'Level',
				'status' => 'Status',
				'end_date' => 'End Date'
		    );
		      $this->cezpdf->ezTable($pdf_data, $col_names, 'Registered Managers', array('width'=>550));
     		 $this->cezpdf->ezStream();
		}
    }
    public function owners()
    {
    	
    	
    	$data['title'] = 'Club Ownership';
	    $this->load->view('admin/heda',$data);
	    $this->load->view('admin/owner_report',$data);

    }
     public function pass_change()
    {
      $this->form_validation->set_rules('pass','Password','xss_clean|trim|required|min_length[6]');
        $this->form_validation->set_rules('con_pass','Confirm Password','xss_clean|trim|required|matches[pass]|min_length[6]');
    
    $this->form_validation->set_rules('c_pass','Password','xss_clean|trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->load->view('admin/heda');
      $this->load->view('admin/a_settings');
    }
    else{
    	$password="";
    	$cur_pass=$this->input->post('c_pass');
        $query=$this->db->get('admin');
        foreach ($query->result() as $row ) {
          $password=$row->password;
        }
        
        if ($password != md5($cur_pass)) {
          $data['error'] = 'sorry the current password is incorrect';
        }
        else{
          $new_pass=$this->input->post('pass');

        $update_data = array('password' => md5($new_pass));

        $result=$this->db->update('admin',$update_data);
        if ($result) {
          $data['success'] = 'password changed successfully';
           
        }
        else{
          $data['error'] = 'sorry an error occurred, try again later';
        }

        }
        
        $this->load->view('admin/heda',$data);
            $this->load->view('admin/a_settings',$data);


        
    }
  }
    

	      

 
}
?>