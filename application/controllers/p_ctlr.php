<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    /**
    * 
    */
    class P_ctlr extends CI_Controller
    {
	function index(){
		$this->load->view('player/player');
	}
	public function p_login(){
		$this->form_validation->set_rules('username','Username','xss_clean|trim|required');
		$this->form_validation->set_rules('password','Password','xss_clean|trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('player/player');
		}
		else{
			$username=$this->input->post('username');
			$password=md5($this->input->post('password'));
			$result=$this->p_model->p_login($username,$password);
			if ($result) {
				$this->p_request();
			}
			else{
						
				$data["error"]="incorrect username or password";
				$this->load->view('player/player',$data);
				
			}
		}
		
	}
	function p_apply(){
		$data['title'] = 'Apply For Transfer';
		$this->load->view('player/p_heda', $data);
		$this->load->view('player/p_apply', $data);
	}
     function p_settings(){
    	$data['title'] = 'System Settings';
    	$this->load->view('player/p_heda',$data);
    	$this->load->view('player/p_settings');
    }
   
    function p_request()
     {
 
       
        
        $data['title'] = "View Transfer Requests";
        $this->load->view('player/p_heda', $data);
        $this->load->view('player/p_request', $data);
    }
    
    function p_search()
     {
        //set table id in table open tag
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="1"  class="mytable">');
        $this->table->set_template($tmpl);
 
        $this->table->set_heading('Club Name', 'Salary', 'Manager', 'Action');//toa salary
        
        $data['title'] = "View Transfer Requests";
        $this->load->view('player/p_heda', $data);
        $this->load->view('player/p_search', $data);
    }
        public function p_nego_ok($id)
        {
            $query = $this->db->get_where('negotiation',array('id' => $id ));
            foreach ($query->result() as $row) {
                $reply = $row->m_reply;
                $dest_club = $row->dest_club;
            }
            if ($reply == 'REJECTED') {
                 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $dest_club . ', click reject to terminate</strong></div><br><br><br><br>');
            }
            else if (empty($reply)) {
                 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>please wait for a reply from ' . $dest_club. '</strong></div><br><br><br><br>');
            }
            else{

            $update_data = array(
                'p_reply' => 'OK'
                );
             $this->db->where('id',$id);
            $this->db->update('negotiation',$update_data);
             $this->session->set_flashdata('ok', '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer accepted successfully</strong></div><br><br><br><br>');
            }
            redirect('p_request');
        }
             public function p_nego_cancel($id)
        {
            $query = $this->db->get_where('negotiation',array('id' => $id ));
            foreach ($query->result() as $row) {
                $reply = $row->m_reply;
                $dest_club = $row->dest_club;
            }
            // if ($reply == 'REJECTED') {
            //      $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
            //                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            //                       <strong>Transfer was terminated by ' . $dest_club . ', click reject to terminate</strong></div>');
            //       redirect('p_request');
            // }
             if (empty($reply)) {
                 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>please wait for a reply from ' . $dest_club . '</strong></div><br><br><br><br>');
                  redirect('p_request');
            }
            else{

            $update_data = array(
                'p_reply' => 'REJECTED'
                );
            $this->db->where('id',$id);
            $this->db->update('negotiation',$update_data);
             // $this->session->set_flashdata('ok', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
             //                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             //                      <strong>Transfer terminated successfully</strong></div>');
             echo '<script> 
            alert("Transfer terminated successfully");
            window.location.href="'.base_url('p_request').'";
            </script>';
            }
           
        }
         public function p_nego_edit($id)
        {
            $query = $this->db->get_where('negotiation',array('id' => $id ));
            foreach ($query->result() as $row) {
                $reply = $row->m_reply;
                $dest_club = $row->dest_club;
            }
            if ($reply == 'REJECTED') {
                 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>Transfer was terminated by ' . $dest_club . ', click reject to terminate</strong></div><br><br><br><br>');
                  redirect('p_request');
            }
            else if (empty($reply)) {
                 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>please wait for a reply from ' . $dest_club .  '</strong></div><br><br><br><br>');
                  redirect('p_request');
            }
                else{
        $data['p_nego'] = $this->p_model->nego_edit($id);   
        $result = $data['p_nego'];
        if ($result) {
                $data['title'] = "Transfer Negotiations";
                $this->load->view('player/p_heda', $data);
                $this->load->view('player/p_nego_edit', $data);
               
            }
            else{
                
                 $this->session->set_flashdata('error', '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>unable to compete request </strong></div><br><br><br><br>');
                 redirect('p_request');

            }
            }
           
        }
         public function nego_edit_save()
    {
        $result = $this->p_model->nego_edit_save();
        if ($result) {
            $data['nego_success'] = '<div class="alert alert-success alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>salary successfully updated, ' . anchor('p_request','return to negotiations</strong></div><br><br><br><br>');
             $data['title'] = "Salary Negotiations";
            $this->load->view('player/p_heda', $data);
            $this->load->view('player/p_nego_edit', $data);
        }
        else{
            $data['nego_error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured, '. anchor('p_request','try again</strong></div><br><br><br><br>');
             $data['title'] = "Salary Negotiations";
            $this->load->view('player/p_heda', $data);
            $this->load->view('player/p_nego_edit', $data);
        }
    }
    public function p_apply_save()
    {
        $player = $this->session->userdata('player_sess');
        $club = $player['club_id'];
        $player_id = $player['player_id'];
        $reason = $this->input->post('reason');
        $query = $this->db->get_where('transfer_request', array('player_id' =>$player_id));
        if ($query->num_rows()>0) {
            $data['error'] = '<div class="alert alert-info alert-dismissible col-sm-12" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>you already submitted your application, check the response</strong></div><br><br><br>';
        }
        else{
        $req_save = array(
            'club' => $club , 
            'player_id' => $player_id,
            'reason' => $reason,
            'date' => date('Y-m-d H:i:s')
            );
        $success = $this->db->insert('transfer_request',$req_save);
        if ($success) {
            $data['success'] = '<div class="alert alert-info alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>apllication successfully made</strong></div><br><br><br><br>';
        }
        else{
            $data['error'] = '<div class="alert alert-danger alert-dismissible col-sm-6" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>an error occured, try again later</strong></div><br><br><br>';
        }
        }
        $data['title'] = 'Apply For Transfer';
        $this->load->view('player/p_heda', $data);
        $this->load->view('player/p_apply', $data);
    }
    public function tr_reply()
    {
        $player = $this->session->userdata('player_sess');
$player_id = $player['player_id'];
$success=$error=$reply="";
    $query = $this->db->get_where('transfer_request',array('player_id' => $player_id));
    if ($query->num_rows()>0) {
        foreach ( $query->result() as $row) {
       $reply = $row->reply;
    }
    
    if ($reply == 'ACCEPTED') {
        $data['success'] = '<div class="alert alert-success alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>your request was successfully granted</strong></div><br><br><br><br>';
    }
    else if ($reply == 'REJECTED') {
       $data['error ']= '<div class="alert alert-info alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>sorry, your request was rejected</strong></div><br><br><br><br>';
    }
    else{
        $data['success'] = '<div class="alert alert-info alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>your request is still being processed</strong></div><br><br><br><br>';
    }
    }
    else{
        $data['success'] = '<div class="alert alert-info alert-dismissible col-sm-8" role="alert" >
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <strong>you have not yet applied for any transfer</strong></div><br><br><br><br>';
    }
     
        $data['title'] = 'Transfer Reply';
        $this->load->view('player/p_heda', $data);
        $this->load->view('player/tr_reply', $data);
    }
    public function test()
    {
            $data['query'] = $this->db->get("player");
         $this->load->view('player/p_heda');
         $this->load->view('player/test',$data);
    }
    public function p_req_report()
    {
      $data['title'] = 'Transfer Replies';
        $this->load->view('player/p_heda', $data);
        $this->load->view('player/p_req_report', $data);
    }
    public function pass_change()
    {
      $this->form_validation->set_rules('pass','Password','xss_clean|trim|required|min_length[6]');
        $this->form_validation->set_rules('con_pass','Confirm Password','xss_clean|trim|required|matches[pass]|min_length[6]');
    
    $this->form_validation->set_rules('c_pass','Password','xss_clean|trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->load->view('player/p_heda');
      $this->load->view('player/p_settings');
    }
    else{
      $player = $this->session->userdata('player_sess');
        $player_id = $player['player_id'];
        $this->db->where('player_id',$player_id);
        $query=$this->db->get('player');
        foreach ($query->result() as $row ) {
          $password=$row->password;
        }
        $cur_pass=$this->input->post('c_pass');
        if ($password != md5($cur_pass)) {
          $data['error'] = 'sorry the current password is incorrect';
        }
        else{
          $new_pass=$this->input->post('pass');

        $update_data = array('password' => md5($new_pass));

        $this->db->where('player_id',$player_id);
        $result=$this->db->update('player',$update_data);
        if ($result) {
          $data['success'] = 'password changed successfully';
           
        }
        else{
          $data['error'] = 'sorry an error occurred, try again later';
        }

        }
        
        $this->load->view('player/p_heda',$data);
            $this->load->view('player/p_settings',$data);


        
    }
  }


   
  
}
?>