<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "c_ctlr";
$route['404_override'] = '';
$route['m'] = "m_ctlr/index";
$route['a'] = "a_ctlr/index";
$route['p'] = "p_ctlr/index";
$route['index'] = "c_ctlr/index";
$route['c_reg'] = "a_ctlr/club";
$route['m_reg'] = "a_ctlr/m_reg";
$route['c_edit'] = "a_ctlr/c_edit"; 
$route['p_reg'] = "m_ctlr/player";
$route['p_trans_reg'] = "m_ctlr/p_trans_reg";
$route['apps'] = "m_ctlr/tr_apps_disp";
$route['p_edit'] = "m_ctlr/p_edit"; 
$route['tr_list'] = "m_ctlr/tr_list";
$route['buy'] = "m_ctlr/buy_player";
$route['nego'] = "m_ctlr/tr_nego_disp";
$route['m_settings'] = "m_ctlr/m_settings"; 
$route['renew'] = "m_ctlr/renew";
$route['m_manage_disp'] = "a_ctlr/m_manage_disp";
$route['a_settings'] = "a_ctlr/a_settings";
$route['logout'] = "c_ctlr/logout";
$route['p_request'] = "p_ctlr/p_request";
$route['p_search'] = "p_ctlr/p_search";
$route['p_apply'] = "p_ctlr/p_apply";
$route['p_settings'] = "p_ctlr/p_settings";
// $route['c_update/(:num)'] = "a_ctlr/c_update"; 
$route['c_reports'] = "a_ctlr/c_report"; 
$route['m_report'] = "a_ctlr/m_report"; 
$route['admin_tr_reports'] = "a_ctlr/admin_tr_reports";
$route['contact'] = 'c_ctlr/contact';
$route['p_nego'] = "m_ctlr/p_nego_disp"; 
$route['tr_requests'] = "m_ctlr/tr_requests";
$route['tr_reply'] = "p_ctlr/tr_reply";
$route['owners'] = "a_ctlr/owners";
$route['admin_p_report'] = "a_ctlr/admin_p_report";
$route['manager_p_report'] = "m_ctlr/manager_p_report";
$route['players_bought'] = "m_ctlr/players_bought";
$route['players_sold'] = "m_ctlr/players_sold";
$route['players_prof'] = "m_ctlr/players_prof";
$route['p_req_report'] = "p_ctlr/p_req_report";
 