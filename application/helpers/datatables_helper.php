<?php

function get_buttons($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a class="act_img" href="' . base_url() . 'subscriber/edit/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'subscriber/delete/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function c_helper($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    // $html .= '<a class="act_img" href="' . base_url() . 'a_ctlr/c_get/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'a_ctlr/m_assign/' . $id . '"><img src="' . base_url() . 'images/user.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'a_ctlr/c_delete/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function m_helper($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
     $html .= '<a class="act_img" href="' . base_url() . 'a_ctlr/m_get/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'a_ctlr/m_delete/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function p_helper($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/p_get/' . $id . '"><img src="' . base_url() . 'images/transfer.jpg"/></a>';
    $html .= '</span>';
 
    return $html;
}
function p_buy_helper($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/p_buy_apply/' . $id . '"><img src="' . base_url() . 'images/apply.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function tr_nego_act($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
     $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/nego_ok/' . $id . '"><img src="' . base_url() . 'images/ok.png"/></a>';
     $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/nego_edit/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/nego_cancel/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function p_list($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/list_edit/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/list_remove/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function buy_act($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
     $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/buy_ok/' . $id . '"><img src="' . base_url() . 'images/ok.png"/></a>';
     $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/buy_edit/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/buy_cancel/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function m_nego_act($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
     $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/m_nego_ok/' . $id . '"><img src="' . base_url() . 'images/ok.png"/></a>';
     $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/m_nego_edit/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/m_nego_cancel/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function p_nego_act($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a class="act_img" href="' . base_url() . 'p_ctlr/p_nego_ok/' . $id . '"><img src="' . base_url() . 'images/ok.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'p_ctlr/p_nego_edit/' . $id . '"><img src="' . base_url() . 'images/edit.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'p_ctlr/p_nego_cancel/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}
function m_req_act($id)
{
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/m_req_ok/' . $id . '"><img src="' . base_url() . 'images/ok.png"/></a>';
    $html .= '<a class="act_img" href="' . base_url() . 'm_ctlr/m_req_reject/' . $id . '"><img src="' . base_url() . 'images/delete.png"/></a>';
    $html .= '</span>';
 
    return $html;
}